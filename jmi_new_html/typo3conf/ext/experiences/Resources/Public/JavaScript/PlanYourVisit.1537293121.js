var MSI = MSI || {};

MSI.PlanYourVisit = {
	setDuration: function(duration) {
		$('[data-plan-your-visit] [data-slider]').foundation('slider', 'set_value', duration);
	}
};

$(document).ready(function($) {

	// On normal form submit or pjax, fire off the appropriate pjax request
	$('[data-plan-your-visit] form').on('submit, pjaxsubmit', function(event) {
		$.pjax.submit(event, '[data-plan-your-visit] [data-search-results]', {fragment: '[data-plan-your-visit] [data-search-results]', scrollTo: false});
	});

	// When a form element changes, fire pjax
	$('[data-plan-your-visit] form [data-update-on-change]').on('change', function() {
		var form = $(this).parents('form')[0];
		$(form).trigger('pjaxsubmit');
	});

	// When a form element changes, fire pjax
	$('[data-plan-your-visit] form [data-update-on-click]').on('click', function() {
		var form = $(this).parents('form')[0];
		$(form).trigger('pjaxsubmit');
	});


	// Add keyboard navigation to range picker.
	$('[data-plan-your-visit] [data-slider-handle], [data-duration]').on('keydown', function(event) {
		var currentPosition = parseInt($('[data-plan-your-visit]  [data-slider]').attr('data-slider'));
		switch(event.keyCode) {
			// Arrow Left
            // Arrow Up
			case 37:
			case 38:
				if (currentPosition > 1) {
					MSI.PlanYourVisit.setDuration(currentPosition - 1)
				}
				event.preventDefault();
				break;
			// Arrow Right
            // Arrow Down
			case 39:
			case 40:
				if (currentPosition < 8) {
					MSI.PlanYourVisit.setDuration(currentPosition + 1)
				}
				event.preventDefault();
				break;
		}

	});

	$('[data-plan-your-visit] [data-slider]').on('change.fndtn.slider', function(){
		var value = parseInt($(this).attr('data-slider'));
		var currentValue = $('[data-plan-your-visit] [data-duration-in-minutes]').val();
		$durationSelector = $('#duration-' + value);

		// If the active class has changed, process.
		if (!$durationSelector.hasClass('active') || !currentValue) {
			$('[data-plan-your-visit] .duration').each(function() {
				$(this).removeClass('above').removeClass('below').removeClass('active');

				var duration = parseInt($(this).data('duration'));
				if (duration === value) {
					$(this).addClass('active');
				} else if (duration < value) {
					$(this).addClass('above');
				} else {
					$(this).addClass('below');
				}
			});

			var valueInMinutes = value * 60;
			if (value === 8) {
				valueInMinutes = null;
			}
			$('[data-plan-your-visit] [data-duration-in-minutes]').val(valueInMinutes);

			var form = $(this).parents('form')[0];
			$(form).trigger('pjaxsubmit');
		}
	});

	// Fade the container back while AJAX is underway
	$('[data-plan-your-visit] [data-search-results]').on('pjax:send', function(event, xhr, options) {
		options.container.css('opacity', 0.1);
	});

	// Return to normal opacity when complete.
	$('[data-plan-your-visit] [data-search-results]').on('pjax:complete', function(event, xhr, textStatus, options) {
		options.container.css('opacity', 1);
		MSI.Animations.reset();
	});

	$('[data-duration]').click(function() {
		$element = $(this).find('.time');
		$element.addClass('animated activate').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$element.removeClass('animated activate');
		});
	});

    $('[href=#step1], [href=#step2], [href=#step3], [href=#step4]').keydown(function(e) {
        if (e.keyCode === 13) {
            var $tab = $('[data-plan-your-visit] [data-tab] a[href=' + $(this).attr('href') + ']');
    		if ($(this).is('[data-plan-visit-nav]')) {
    			var $tab = $('[data-plan-your-visit] [data-tab] a[href=' + $(this).attr('href') + ']');
    			$tab.trigger('click');
    			return false;
    		} else {
    			deactivateActiveTab();
    			activateTab($tab.parent());
    		}
        }
    });

	$('[href=#step1], [href=#step2], [href=#step3], [href=#step4]').click(function(e) {
		var $tab = $('[data-plan-your-visit] [data-tab] a[href=' + $(this).attr('href') + ']');
		if ($(this).is('[data-plan-visit-nav]')) {
			var $tab = $('[data-plan-your-visit] [data-tab] a[href=' + $(this).attr('href') + ']');
			$tab.trigger('click');
			return false;
		} else {
			deactivateActiveTab();
			activateTab($tab.parent());
		}
	});

	$('[data-plan-your-visit] .tab-title').click(function() {
		deactivateActiveTab();
	});

	function activateTab($element) {
		$element.addClass('animated activate').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $('[data-plan-your-visit] .tab-title a').attr('tabindex', -1);
            $('[data-plan-your-visit] [role=tabpanel]').attr('aria-hidden', false);

			$(this).removeClass('animated activate');
		});
	}

	function deactivateActiveTab() {
		$('[data-plan-your-visit] .tab-title.active').addClass('animated deactivate').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$('[data-plan-your-visit] .tab-title a').attr('tabindex', -1);
            $('[data-plan-your-visit] [role=tabpanel]').attr('aria-hidden', false);

			$(this).removeClass('animated deactivate');
		});
	}

	// If there's a visit date in the cart, initialize plan your visit with it.
	// @todo @ticketure Switch over to MSI.User.Ticketure eventually.
	var cartVisitDate = MSI.User.getCartVisitDate();
	if (cartVisitDate) {
		var formattedDate = moment.tz(new Date(cartVisitDate), 'America/Chicago').format('M/D/YYYY');
		$('[data-plan-your-visit] #datepicker').val(formattedDate);
		var form = $('[data-plan-your-visit] form')[0];
		$(form).trigger('pjaxsubmit');
	}

	$('.tabs-plan-visit-tool-wrap [data-tab] a').click(function(){
		var current = $('[data-plan-your-visit] .content.active').attr('id');
		var currentNum = parseInt(current.substr(current.length-1));
		var next = $(this).attr('href');
		var nextNum = parseInt(next.substr(next.length-1));

		var lastActiveClasses;
		if (currentNum > nextNum) {
			lastActiveClasses = 'last-active slideOutLeft';
			$(next).addClass('slideInLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				$(this).removeClass('slideInLeft');
			});
		} else {
			lastActiveClasses = 'last-active slideOutRight';
			$(next).addClass('slideInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				$(this).removeClass('slideInRight');
			});
		}

		$('[data-plan-your-visit] .content.active').addClass(lastActiveClasses).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$(this).removeClass(lastActiveClasses);
		});

		if (!$('html').hasClass('cssanimations')) {
			$('[data-plan-your-visit] .content').removeClass(lastActiveClasses);
		}

	});
});
