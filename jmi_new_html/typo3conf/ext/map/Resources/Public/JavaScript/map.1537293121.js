// setup global variables
var map;
var gMap;
var mapCenter = new google.maps.LatLng(41.790665, -87.582846);
var responsiveCenter;
var dynamicFeatures = [];
var staticFeatures = [];
var infowindow = new google.maps.InfoWindow({zIndex: 999, maxWidth: 300});
var isDesktop = typeof window.orientation === "undefined";
var mapHeight = String(window.innerHeight - 90) + 'px';

var geoLoc;
var marker;
var watchId;
var startPos;
var endPos;
var increments = 100;
var incrementCount = 0;
var latIncrement;
var longIncrement;
var delay = 10; //milliseconds

$('#feature').addClass('map');

dynamicFeatures['1'] = new google.maps.Data();
dynamicFeatures['2'] = new google.maps.Data();
dynamicFeatures['3'] = new google.maps.Data();
dynamicFeatures['4'] = new google.maps.Data();
staticFeatures['1'] = new google.maps.Data();
staticFeatures['2'] = new google.maps.Data();
staticFeatures['3'] = new google.maps.Data();
staticFeatures['4'] = new google.maps.Data();

// load markers from geojson files
for (var i=1; i<5; i++) {
  var staticJsonFolder;
  var floorNum = String(i);
  var dynamicPath = $('[data-dynamic-features-' +floorNum+ ']').attr('data-dynamic-features-' +floorNum);

  switch (floorNum) {
    case '1':
      staticJsonFolder = 'entry';
      break;
    case '2':
      staticJsonFolder = 'lower';
      break;
    case '3':
      staticJsonFolder = 'main';
      break;
    case '4':
      staticJsonFolder = 'balcony';
      break;
  }

  dynamicFeatures[floorNum].loadGeoJson(dynamicPath);
  staticFeatures[floorNum].loadGeoJson('/typo3conf/ext/map/Resources/Public/Json/' +staticJsonFolder+ '/static.geo.json');
}

// setup the map styles
var mapStyles = [
  {
    "featureType": "administrative",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "poi",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "transit",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road",
    "stylers": [
      { "visibility": "on" },
      { "color": "#ffffff"}
    ]
  },{
    "elementType": "labels.text.fill",
    "featureType": "road",
    "stylers": [
      { "visibility": "on" },
      { "color": "#b6b6b6"}
    ]
  },{
    "elementType": "labels.text.stroke",
    "featureType": "road",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "landscape",
    "stylers": [
      { "color": "#e6e6e6" }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "visibility": "on" },
      { "color": "#2aa9e0" }
    ]
  }
];

var styledMap = new google.maps.StyledMapType(mapStyles, {name: "MSI Map Style"});
styledMap.tileSize = new google.maps.Size(256, 256);

// determine if device is retina and assign base path to overlays accordingly
var isRetina = (
	window.devicePixelRatio > 1 ||
	(window.matchMedia && window.matchMedia("(-webkit-min-device-pixel-ratio: 1.5),(-moz-min-device-pixel-ratio: 1.5),(min-device-pixel-ratio: 1.5)").matches)
);

if (isRetina === true) {
  var basePath = '/typo3conf/ext/map/Resources/Public/MapImages/retina';
} else {
  var basePath = '/typo3conf/ext/map/Resources/Public/MapImages/standard';
}

// set up the map overlays
var entryLevelOverlay = new google.maps.ImageMapType({
      getTileUrl: function(coord, zoom) {
        return basePath + '/entry' + '/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
      },
      tileSize: new google.maps.Size(256, 256)
});
var lowerLevelOverlay = new google.maps.ImageMapType({
      getTileUrl: function(coord, zoom) {
        return basePath + '/lower' + '/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
      },
      tileSize: new google.maps.Size(256, 256)
});
var mainLevelOverlay = new google.maps.ImageMapType({
      getTileUrl: function(coord, zoom) {
        return basePath + '/main' + '/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
      },
      tileSize: new google.maps.Size(256, 256)
});
var balconyLevelOverlay = new google.maps.ImageMapType({
      getTileUrl: function(coord, zoom) {
        return basePath + '/balcony' + '/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
      },
      tileSize: new google.maps.Size(256, 256)
});

// When the user clicks an experience in the list, switch to the appropriate floor initialized with the selected experience
$('#interactive-map .map-sidebar div[data-id]').click(function() {
  var id = $(this).attr('data-id');
  var floorNum = $(this).attr('data-floor');
  changeFloor(floorNum, id);
});

// Show/hide the legend
$('[data-map-legend-trigger]').click(function() {
  var buttonMargin = parseInt($('#map-legend-button').css('margin-top'));
  if ($(this).attr('data-map-legend-trigger') == 'closed') {
    $(this).attr('data-map-legend-trigger', 'open');
    $('#map-legend').show(500, function() {
      var mainContentHeight = $('#interactive-map .map-main-content').height();
      $('#interactive-map .map-sidebar').height(mainContentHeight + buttonMargin);
    });

    $('#map-legend-button i').removeClass('icon-anglebracket-up');
    $('#map-legend-button i').addClass('icon-anglebracket-down');
  } else {
    $(this).attr('data-map-legend-trigger', 'closed');
    $('#map-legend').hide(500, function() {
      var mainContentHeight = $('#interactive-map .map-main-content').height();
      $('#interactive-map .map-sidebar').height(mainContentHeight + buttonMargin);
    });

    $('#map-legend-button i').removeClass('icon-anglebracket-down');
    $('#map-legend-button i').addClass('icon-anglebracket-up');
  }
});

// Load the map when the page loads
google.maps.event.addDomListener(window, 'load', loadMap);

google.maps.event.addListener(infowindow, 'domready', function() {
  console.log('domready');
  console.log($('#museum-map .info-window .box-teaser'), "inside");
  MSI.UI.registerBoxTeaserEventHandler($('#museum-map .info-window .box-teaser'));
});



/**
 * Load the map onto the page
 *
 */
function loadMap() {
	var mapZoom = 18;
	var mapZoomMax = 20;
	var mapZoomMin = 10;

	//These options configure the setup of the map.
	var mapOptions = {
		center: mapCenter,
		zoom: mapZoom,
		maxZoom:mapZoomMax,
		minZoom:mapZoomMin,
		panControl: false,
    scrollwheel: false,
		mapTypeControl: false,
		mapTypeControlOptions: {
			mapTypeIds: [ 'map_styles']
		},
    disableDefaultUI: true
	};

  // set the map's height according to window innerHeight
  if (!isDesktop) {
    $('#museum-map').css({height: mapHeight});
  }

  // create the map with options and styling
  map = new google.maps.Map(document.querySelector("[data-museum-map]"), mapOptions);
	map.mapTypes.set('map_styles', styledMap);
	map.setMapTypeId('map_styles');

  // load with correct floor and initialize selected experience if applicable
  var onLoadSelection = $('#interactive-map .map-sidebar div[data-active-on-load]');
  var id = null;
  var floorNum = '2';
  if ($('.map-sidebar [data-floor=2]').length === 0) {
    floorNum = $('.map-sidebar [data-floor]').attr('data-floor');
  }
  if (onLoadSelection.length) {
    id = onLoadSelection.attr('data-id');
    floorNum = onLoadSelection.attr('data-floor');
  }
  changeFloor(floorNum, id);

  // setup controls and make visible after tiles have loaded
  var tilesLoadedCounter = 0;
  google.maps.event.addListener(map, 'tilesloaded', function(evt) {
    tilesLoadedCounter++;
    if (tilesLoadedCounter == 1) {
      var zoomControlDiv = $('[data-zoom-control]').get(0);
      var zoomControl = new ZoomControl();
      var floorControlDiv = $('[data-floor-control]').get(0);
      var floorControl = new FloorControl();
      var geolocationControlDiv = $('[data-geolocation-control]').get(0);
      var geolocationControl = new GeolocationControl();

      if (isDesktop) {
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
        $('[data-zoom-control]').css('display', 'block');

        map.controls[google.maps.ControlPosition.LEFT_TOP].push(floorControlDiv);
        $('[data-floor-control]').css('display', 'block');
      } else {
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(floorControlDiv);
        $('[data-floor-control]').css('display', 'block');

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(geolocationControlDiv);
        $('[data-geolocation-control]').css('display', 'block');
      }
    }
  });

  // alter the look of the infowindow
  google.maps.event.addListener(infowindow, 'domready', function() {
    var iwOuter = $('.gm-style-iw');
    var iwBackground = iwOuter.prev();
    var iwParent = iwBackground.parent();
    var iwCloseBtn = iwOuter.next();

    iwOuter.css({
      top: '115px',
      left: '24px'
    });

    iwCloseBtn.css({
      display: 'none'
    });

    iwBackground.css({'display' : 'none'});
  });

  setMapHeight();

  // When the window is resized, recenter to make responsive
  google.maps.event.addDomListener(map, 'idle', function() {
    calculateCenter();
  });
  google.maps.event.addDomListener(window, 'resize', function() {
    map.setCenter(responsiveCenter);
    setMapHeight();
  });

  // listen for zoom change and hide markers at certain zoom levels
  google.maps.event.addListener(map, "zoom_changed", function() {
    var floorNum = $('.floor-button-container.active').attr('data-floor-control-level');

    if (map.getZoom() < 17) {
      removeFeatures(staticFeatures);
    } else if (staticFeatures[floorNum].getMap() === null) {
      addFeatures(staticFeatures[floorNum], 1);
    }

    if (map.getZoom() < 16) {
      removeFeatures(dynamicFeatures);
    } else if (dynamicFeatures[floorNum].getMap() === null) {
      addFeatures(dynamicFeatures[floorNum], 1500);
    }
  });

}

/**
 * Change floors
 * @param string floorNum
 * @param string activeId (the id of the experience that is active on load)
 *
 * @return void
 */
function changeFloor(floorNum, activeId) {
  if(map.overlayMapTypes.length > 1) {
    map.overlayMapTypes.removeAt(0);
  }

  $('[data-floor-control-level].active, [data-floor-control-level] div.active, [data-floor-control-level] span.active').removeClass('active');
  $('[data-floor-control-level=' +floorNum+ '], [data-floor-control-level=' +floorNum+ '] div, [data-floor-control-level=' +floorNum+ '] span').addClass('active');

  infowindow.close();

  var overlay;
  var staticJsonFolder;
  switch (floorNum) {
    case '1':
      overlay = entryLevelOverlay;
      staticJsonFolder = 'entry';
      break;
    case '2':
      overlay = lowerLevelOverlay;
      staticJsonFolder = 'lower';
      break;
    case '3':
      overlay = mainLevelOverlay;
      staticJsonFolder = 'main';
      break;
    case '4':
      overlay = balconyLevelOverlay;
      staticJsonFolder = 'balcony';
      break;
  }

  // add new overlays
  var length = map.overlayMapTypes.length;
  map.overlayMapTypes.insertAt(length, overlay);

  // remove features currently on map
  removeFeatures(staticFeatures);
  removeFeatures(dynamicFeatures);

  // assign markers for current floor to the map
  addFeatures(staticFeatures[floorNum], 1);
  addFeatures(dynamicFeatures[floorNum], 1500);

  // only display sidebar items for the current floor
  /*
  for (var x=1;x<5;x++) {
    if (x == floorNum) {
      $(".map-sidebar div[data-floor=" + x + "]").show();
    } else {
      $(".map-sidebar div[data-floor=" + x + "]").hide();
    }
  }
  */

  // initialize with activeId if applicable
  if (activeId) {
    var content = $("[data-info-window-id='" +activeId+ "']").html();
    var feature = dynamicFeatures[floorNum].getFeatureById(activeId);
    if (feature) {
      showInfoWindow(content, feature);
    } else {
      dynamicFeatures[floorNum].addListener('addfeature', function(event) {
        var id = event.feature.getId();
        if (id == activeId) {
          showInfoWindow(content, event.feature);
        }
      });
    }
  }

  // When the user clicks a marker, open an infowindow. We must also clear any previous listeners
  google.maps.event.clearListeners(dynamicFeatures[floorNum], 'click');
  google.maps.event.addListener(dynamicFeatures[floorNum], 'click', function(event) {
    var id = event.feature.getId();
    var content = $("[data-info-window-id='" +id+ "']").html();
    showInfoWindow(content, event.feature);
  });

  // When the user clicks the map, if the infowindow is open, then close it
  google.maps.event.addListener(map, "click", function(event) {
    infowindow.close();
  });
}

/**
 * Calculate the current center of the map
 *
 */
function calculateCenter() {
  responsiveCenter = map.getCenter();
}

/**
 * Set the map height based on current aspect ratio
 *
 */
function setMapHeight() {
  if (window.matchMedia("only screen and (max-width: 768px)").matches && window.innerHeight < 790) {
    mapHeight = String(window.innerHeight - 90) + 'px';
    $('#museum-map').css({height: mapHeight});
    $('#footer').hide();
  } else {
    $('#museum-map').css({height: '700px'});
    $('#footer').show();
  }
}

/**
 * The ZoomControl adds +/- button for the map
 *
 */
function ZoomControl() {
  var zoomInButton = $('[data-zoom-in]').get(0);
  var zoomOutButton = $('[data-zoom-out]').get(0);

  // Setup the click event listener - zoomIn
  google.maps.event.addDomListener(zoomInButton, 'click', function() {
    map.setZoom(map.getZoom() + 1);
  });

  // Setup the click event listener - zoomOut
  google.maps.event.addDomListener(zoomOutButton, 'click', function() {
    map.setZoom(map.getZoom() - 1);
  });
}

/**
 * The FloorControl
 *
 */
function FloorControl() {
  // Setup the click event listener - floor change
  $('[data-floor-control-level]').click(function() {
    var floorNum = $(this).attr('data-floor-control-level');
    changeFloor(String(floorNum));
  });
}

/**
 * The Geolocation Control
 *
 */
function GeolocationControl() {
  // Setup the click event listener - geolocation
  $('[data-geolocation-control-container]').click(function() {
    if($(this).hasClass('active')) {
      geoLoc.clearWatch(watchId);
      $('#altitude-container').html('');
      map.setCenter(mapCenter);
      $(this).toggleClass('active');
    } else {
      showMyLocation();
      $(this).toggleClass('active');
    }
  });
}

/**
 * Show Info Window
 * @param string HTML content to display in info window
 * @param obj marker to anchor info window to
 * @return void
 *
 */
function showInfoWindow(content, feature) {
  var lat = feature.getGeometry().get().lat();
  var lng = feature.getGeometry().get().lng();
  var latOffset = 0;
  var lngOffset = 0;
  var pixelOffsetX = -33;
  var pixelOffsetY = -60;
  var panByX = 0;
  var panByY = 100;

  switch(map.getZoom()) {
    case 20:
      latOffset = 0.00028;
      break;
    case 19:
      latOffset = 0.0006;
      break;
    case 18:
      latOffset = 0.0012;
      break;
    case 17:
      latOffset = 0.0018;
      break;
    case 16:
      latOffset = 0.0024;
      break;
    default:
      latOffset = 0.0012;
  }

  if (window.matchMedia("only screen and (max-width: 599px)").matches) {
    if (window.matchMedia("only screen and (orientation: landscape)").matches) {
      pixelOffsetX = -120;
      pixelOffsetY = -225;
      panByX = 170;
      panByY = 60;
      if (map.getZoom() < 17) {
        latOffset = latOffset * -0.9;
      } else {
        latOffset = latOffset * -0.6;
      }
    } else {
      pixelOffsetX = -120;
      pixelOffsetY = -75;
      panByX = 170;
      panByY = 60;
      lngOffset = latOffset * -0.1;
      latOffset = latOffset * -0.4;
    }
  } else if (window.matchMedia("only screen and (max-width: 690px)").matches) {
    if (window.matchMedia("only screen and (orientation: landscape)").matches) {
      pixelOffsetX = -190;
      pixelOffsetY = -260;
      panByX = 170;
      panByY = 60;
      latOffset = latOffset * -0.4;
    } else {
      pixelOffsetX = -190;
      pixelOffsetY = -80;
      panByX = 170;
      panByY = 60;
    }
  } else if (window.matchMedia("only screen and (max-width: 767px)").matches) {
    if (window.matchMedia("only screen and (orientation: landscape)").matches) {
      pixelOffsetX = -190;
      pixelOffsetY = -75;
      panByX = 140;
      panByY = 60;
      latOffset = latOffset * -0.2;
    } else {
      pixelOffsetX = -190;
      pixelOffsetY = -75;
      panByX = 185;
      panByY = 100;
      latOffset = latOffset * 0.4;
    }
  } else if (window.matchMedia("only screen and (max-width: 1023px)").matches) {
    pixelOffsetX = -22;
    pixelOffsetY = -10;
    latOffset = latOffset * 0.4;
  }

  infowindow.setContent(content);
  infowindow.setPosition({lat:lat, lng:lng});
  infowindow.setOptions({pixelOffset: new google.maps.Size(pixelOffsetX, pixelOffsetY)});
  infowindow.open(map);

  var $boxTeaser = $(infowindow);
  MSI.UI.registerBoxTeaserEventHandler($('.box-teaser'));

  $('[data-floor-control-level] span.active').removeClass('active');

  if (map.getZoom() == 18) {
    setTimeout(function() {
      map.panBy(panByX,panByY);
    }, 500);
  } else {
    setTimeout(function() {
      map.panTo({lat:lat + latOffset, lng:lng + lngOffset});
    }, 500);
  }

  // Google maps puts a container around our infowindow which we've made invisible.
  // If a user clicks the outside container it should function the same as clicking the map - close the infowindow
  $('.gm-style-iw').parents().first().click(function(event) {
    if (!$.contains( $('.gm-style-iw .info-window').get(0), event.target )) {
      infowindow.close();
    }
  });
}

/**
 * The user's current geolocation
 *
 */
function showMyLocation() {
  if (navigator.geolocation) {
    geoLoc = navigator.geolocation;
    watchId = navigator.geolocation.watchPosition(function(position) {
      endPos = {lat: position.coords.latitude, lng: position.coords.longitude};

      if (marker) {
        incrementCount = 0;
        latIncrement = (endPos.lat - startPos.lat)/increments;
        longIncrement = (endPos.lng - startPos.lng)/increments;
        moveMarker();
      } else {
        startPos = endPos;
        marker = new google.maps.Marker({
          position: startPos,
          icon: '/typo3conf/ext/map/Resources/Public/Icons/geolocation.png',
          map: map
        });
        map.setCenter(endPos);
      }
    }, function(error) {
      console.log('Error occurred. Error code: ' + error.code);
    }, {
      enableHighAccuracy: true,
      timeout: 60000,
      maximumAge: 0
    });
  } else {
    // Browser doesn't support Geolocation
    console.log('browser does not support geolocation');
  }
}

/**
 * Move geolocation marker
 *
 */
function moveMarker() {
    startPos.lat += latIncrement;
    startPos.lng += longIncrement;
    var latlng = new google.maps.LatLng(startPos.lat, startPos.lng);
    var modifiedBounds = getModifiedBounds();

    if (!modifiedBounds.contains(latlng)) {
        map.panTo(latlng);
    }

    marker.setPosition(latlng);
    if(incrementCount != increments){
        incrementCount++;
        setTimeout(moveMarker, delay);
    }
}

/**
 * The Modified Bounds - a percentage of current map bounds
 *
 */
function getModifiedBounds() {
  var bounds = map.getBounds();
  var southWest = {lat: bounds.getSouthWest().lat(), lng: bounds.getSouthWest().lng()};
  var northEast = {lat: bounds.getNorthEast().lat(), lng: bounds.getNorthEast().lng()};
  var latRange = Math.abs(northEast.lat - southWest.lat);
  var lngRange = Math.abs(northEast.lng - southWest.lng);
  var newSouthWestLat = southWest.lat + (latRange*0.35);
  var newSouthWestLng = southWest.lng + (lngRange*0.35);
  var newNorthEastLat = northEast.lat - (latRange*0.35);
  var newNorthEastLng = northEast.lng - (lngRange*0.35);

  return new google.maps.LatLngBounds(
    {lat: newSouthWestLat, lng: newSouthWestLng},
    {lat: newNorthEastLat, lng: newNorthEastLng}
  );
}

/**
 * Remove markers from the map
 * @param array of markers to remove
 * @return void
 *
 */
function removeFeatures(features) {
  for (var i=1; i<5; i++) {
    var floorNum = String(i);
    features[floorNum].setMap(null);
  }
}

/**
 * Add markers to the map
 * @param markers to add (staticFeatures[floorNum] or dynamicFeatures[floorNum])
 * @param zIndex for the icons
 * @return void
 *
 */
function addFeatures(features, zIndex) {
  if (zIndex === undefined) {
    zIndex = 1;
  }

  // assign markers for current floor to the map
  features.setMap(map);

  // style the markers
  features.setStyle(function(feature) {
    var iconPath = '/typo3conf/ext/map/Resources/Public/Icons/' + feature.getProperty('icon');
    return {
      icon:iconPath,
      zIndex: zIndex
    };
  });
}
