SettingsConfiguration = {
    'debugMode': false,
    'mode': 'web', //kiosk or web or mobile
    // @todo Point this back at sandbox and/or production eventually.
    'apiUrl': 'https://sv.msichicago.org/api/',

    // @note Alternate URL for static responses if SmartVisit is unavailable.
    // 'apiUrl': '/typo3conf/ext/msi_ticketing/Resources/Public/SmartVisitMock/',


    'kioskId': 12,

    'features': {
        'mapTestData': false,
        'defaultPromoCode': 'GREATINDOORS'
    }
};
