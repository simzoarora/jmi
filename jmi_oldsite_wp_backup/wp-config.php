<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'luminog_jmi');

/** MySQL database username */
define('DB_USER', 'luminog_jmi');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=4%esAA*_WGTYUfUD>1fEEi$J39i(cDx^W8Om&3rue(pM^wAC{E/b<5K~9 ;opeR');
define('SECURE_AUTH_KEY',  'WYMwZTtQG(8:K(oz|kU`ADo`]h^LUS3E9qNR/c7%HqSJ2N9_fn-75CRXjA!~OFxN');
define('LOGGED_IN_KEY',    'oc,3grMgK!E]o=ayl$#7!^bs:Go_:a,]dke<AV[ytf^fw+of)jZg=onW<$BeRpt?');
define('NONCE_KEY',        '1^dq)CKj>q<rXMSj>*0PygcI,57.|YcM[YRw|9}oFhNW#}|I}/}x8w=~LH@u&{xK');
define('AUTH_SALT',        'rsK,}B~iGJZkz>/]{zRvjcCbkB|&I=XyZ=gw4~z}0iK%h<l]Mcj> #u#dH?c8m,?');
define('SECURE_AUTH_SALT', '$(s8Zg%8^!ScZ{s9sen<:Mc.V|E+Y#1Xp`Q4>,nW}dXBpVEXNEZ(4Gxi/CI!?b/-');
define('LOGGED_IN_SALT',   'f!.8Vk3W3AJr.hp@Q^h*dAW,VBm/)rqBj(=c6%n4whnddL:Se=e$~vXN8_~vk|<t');
define('NONCE_SALT',       '*&?7Ksn=z;ThZSdjk}x}5L@p^T^.<IRIf3@G#r!9w}Yt;8B={zBJk)~O`;.sS?dz');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
