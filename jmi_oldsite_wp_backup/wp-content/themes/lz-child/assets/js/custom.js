function googleTranslateElementInit2() {
    new google.translate.TranslateElement({
        pageLanguage: 'en',
        autoDisplay: false
    }, 'google_translate_element2');
}

function GTranslateGetCurrentLang() {
    var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');
    console.log(keyValue);
    return keyValue ? keyValue[2].split('/')[2] : null;
}

function GTranslateFireEvent(element, event) {
    try {
        if (document.createEventObject) {
            var evt = document.createEventObject();
            element.fireEvent('on' + event, evt)
        } else {
            var evt = document.createEvent('HTMLEvents');
            evt.initEvent(event, true, true);
            element.dispatchEvent(evt)
        }
    } catch (e) {}
}

function doGTranslate(lang_pair) {
    if (lang_pair.value) lang_pair = lang_pair.value;
    if (lang_pair == '') return;
    var lang = lang_pair.split('|')[1];
    if (GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0]) return;
    var teCombo;
    var sel = document.getElementsByTagName('select');
    for (var i = 0; i < sel.length; i++)
        if (/goog-te-combo/.test(sel[i].className)) {
            teCombo = sel[i];
            break;
        }
    if (document.getElementById('google_translate_element2') == null || document.getElementById('google_translate_element2').innerHTML.length == 0 || teCombo.length == 0 || teCombo.innerHTML.length == 0) {
        setTimeout(function () {
            doGTranslate(lang_pair)
        }, 500)
    } else {
        teCombo.value = lang;
        GTranslateFireEvent(teCombo, 'change');
        GTranslateFireEvent(teCombo, 'change')
    }
}

(function ($) {
    $(document).ready(function () {

      //  $(window).bind('load', function () {

            $("li.menu-item, a.nav-link, .sub-menu-container").hover(function () {
                $(this).siblings(".sub-menu-container").css('display', 'block');
            }, function () {
                $(this).siblings(".sub-menu-container").css('display', 'none');
            });


            var percent = 0,
            crsl = $('#myCarousel');
            var paused = 0;

            crsl.carousel({
                interval: 11000,
                pause: 0
            })

            var animationDiv = document.querySelector('.stroke');
            var clickDiv = document.querySelector('.icon');
            var play = document.querySelector('.play');
            var pause = document.querySelector('.pause');
            var length = animationDiv.getTotalLength();
            // This sets the strokes dasharray and offset to be exactly the length of the stroke
            animationDiv.style.strokeDasharray = length;
            animationDiv.style.strokeDashoffset = length;
            // Toggle the animation-play-state of the ".stroke" on clicking the ".icon" -container
            var animationDiv = document.querySelector('.stroke');
            var clickDiv = document.querySelector('.icon');
            var play = document.querySelector('.play');
            var pause = document.querySelector('.pause');
            //on load 
            play.classList.add('hidden');
            pause.classList.remove('hidden');
            animationDiv.style.webkitAnimationPlayState = "running";

            // Change the animation property "animation-play-state" of the ".stroke" from "running" to "paused" on click
            clickDiv.addEventListener('click', function () {
                var animationDiv = document.querySelector('.stroke');
                if (animationDiv.style.webkitAnimationPlayState == "paused" || animationDiv.style.webkitAnimationPlayState == "") {
                    
                    play.classList.add('hidden');
                    pause.classList.remove('hidden');
                    animationDiv.style.webkitAnimationPlayState = "running";
                    $(crsl).carousel('cycle');
                    animationDiv.style.animationDuration = "11s";

                    var newone = animationDiv.cloneNode(true);
                    animationDiv.replaceWith(newone);
                    
                } else if (animationDiv.style.webkitAnimationPlayState == "running") {
                    pause.classList.add('hidden');
                    play.classList.remove('hidden');
                    animationDiv.style.webkitAnimationPlayState = "paused"; // Logging the animation-play-state to the console:
                    $(crsl).carousel('pause');
                    animationDiv.style.animationDuration = "0s";
                }
                //console.log(animationDiv.style.webkitAnimationPlayState);
            })

            $(".banner-slider li").on('click', function() {
                pause.classList.add('hidden');
                play.classList.remove('hidden');
                animationDiv.style.animationDuration = "0s";
                animationDiv.style.webkitAnimationPlayState = "paused";
                $(crsl).carousel('pause');
            })

            //second  slider
            $('#carousel-example').carousel({
                interval: 9000,
                pause: 0
            });

            $('#carousel-example-generic').carousel({
                interval: 9000,
                pause: 0
            });

            var animationDiv2 = document.querySelector('.stroke2');
            var clickDiv2 = document.querySelector('.icon2');
            var play2 = document.querySelector('.play2');
            var pause2 = document.querySelector('.pause2');
             var length = animationDiv2.getTotalLength();
            // This sets the strokes dasharray and offset to be exactly the length of the stroke
            animationDiv2.style.strokeDasharray = length;
            animationDiv2.style.strokeDashoffset = length;
            // Toggle the animation-play-state of the ".stroke" on clicking the ".icon" -container
          
            var clickDiv2 = document.querySelector('.icon2');
            var play2 = document.querySelector('.play2');
            var pause2 = document.querySelector('.pause2');
            //on load 
            play2.classList.add('hidden2');
            pause2.classList.remove('hidden2');
            animationDiv2.style.webkitAnimationPlayState = "running";

            // Change the animation property "animation-play-state" of the ".stroke" from "running" to "paused" on click
            clickDiv2.addEventListener('click', function () {
                var animationDiv2 = document.querySelector('.stroke2');
                if (animationDiv2.style.webkitAnimationPlayState == "paused" || animationDiv2.style.webkitAnimationPlayState == "") {
                    play2.classList.add('hidden2');
                    pause2.classList.remove('hidden2');
                    animationDiv2.style.webkitAnimationPlayState = "running";
                    $("#carousel-example-generic").carousel('cycle');
                    $('#carousel-example').carousel('cycle');
                    animationDiv2.style.animationDuration = "9s";

                    var newone = animationDiv2.cloneNode(true);
                    animationDiv2.replaceWith(newone);

                } else if (animationDiv2.style.webkitAnimationPlayState == "running") {
                    pause2.classList.add('hidden2');
                    play2.classList.remove('hidden2');
                    animationDiv2.style.webkitAnimationPlayState = "paused"; // Logging the animation-play-state to the console:
                    $("#carousel-example-generic").carousel('pause');
                    $('#carousel-example').carousel('pause');
                    animationDiv2.style.animationDuration = "0s";
                }
                // console.log(animationDiv.style.webkitAnimationPlayState);
            })

            //second slider move slide vice versa based on indicators click
            var totalItems = $('.second-slide').length -1;

            $(".second-carousel-slider li").on('click', function () {

                var currentIndex = $(this).index();

                $(".first-carousel-indicators li").removeClass("active");
                console.log(currentIndex);
                if (currentIndex == totalItems ){
                    $('#carousel-example').carousel(0);
                } else if (currentIndex == 0){
                    $('#carousel-example').carousel(1);
                }else{
                    var num = currentIndex + 1;
                    console.log(num);
                    $('#carousel-example').carousel(num);
                }
                pause2.classList.add('hidden2');
                play2.classList.remove('hidden2');
                animationDiv2.style.animationDuration = "0s";
                animationDiv2.style.webkitAnimationPlayState = "paused";
                $("#carousel-example-generic").carousel('pause');
                $('#carousel-example').carousel('pause');
              
            })

        
            //testimonial-slide slider
            $('#myCarouselBottom').carousel({
                interval: 11000,
                pause: 0
            });

            var animationDiv3 = document.querySelector('.stroke3');
            var clickDiv3 = document.querySelector('.icon3');
            var play3 = document.querySelector('.play3');
            var pause3 = document.querySelector('.pause3');
             var length = animationDiv3.getTotalLength();
            // This sets the strokes dasharray and offset to be exactly the length of the stroke
            animationDiv3.style.strokeDasharray = length;
            animationDiv3.style.strokeDashoffset = length;
            // Toggle the animation-play-state of the ".stroke" on clicking the ".icon" -container
            var animationDiv3 = document.querySelector('.stroke3');
            var clickDiv3 = document.querySelector('.icon3');
            var play3 = document.querySelector('.play3');
            var pause3 = document.querySelector('.pause3');
            //on load 
            play3.classList.add('hidden3');
            pause3.classList.remove('hidden3');
            animationDiv3.style.webkitAnimationPlayState = "running";

            // Change the animation property "animation-play-state" of the ".stroke" from "running" to "paused" on click
            clickDiv3.addEventListener('click', function () {
                var animationDiv3 = document.querySelector('.stroke3');
                if (animationDiv3.style.webkitAnimationPlayState == "paused" || animationDiv3.style.webkitAnimationPlayState == "") {
                    play3.classList.add('hidden3');
                    pause3.classList.remove('hidden3');
                    animationDiv3.style.webkitAnimationPlayState = "running";
                    animationDiv3.style.animationDuration = "10s";

                    var newone = animationDiv3.cloneNode(true);
                    animationDiv3.replaceWith(newone);

                    $('#myCarouselBottom').carousel('cycle');
        
                } else if (animationDiv3.style.webkitAnimationPlayState == "running") {
                    pause3.classList.add('hidden3');
                    play3.classList.remove('hidden3');
                    animationDiv3.style.webkitAnimationPlayState = "paused"; // Logging the animation-play-state to the console:
                   
                    animationDiv3.style.animationDuration = "0s";
                  
                    $('#myCarouselBottom').carousel('pause');
                }
                // console.log(animationDiv.style.webkitAnimationPlayState);
            })

            $(".testimonial-slide li").on('click', function () {
                pause3.classList.add('hidden3');
                play3.classList.remove('hidden3');
                animationDiv3.style.animationDuration = "0s";
                animationDiv3.style.webkitAnimationPlayState = "paused";
                $('#myCarouselBottom').carousel('pause');
            })

            $('#second-right').on('click', function (e) {
                e.preventDefault()
                $("#carousel-example , #carousel-example-generic").carousel('next')
            })
             $('#second-left').on('click', function (e) {
                e.preventDefault()
                $("#carousel-example , #carousel-example-generic").carousel('prev')
            })

            //mobile slider on touch
            $("#myCarousel").swiperight(function () {
                $(this).carousel('next');
            });
            $("#myCarousel").swipeleft(function () {
                $(this).carousel('prev');
            });

            $("#carousel-example , #carousel-example-generic").swiperight(function () {
                $(this).carousel('next');
            });
            $("#carousel-example, #carousel-example-generic").swipeleft(function () {
                $(this).carousel('prev');
            });

            $("#myCarouselBottom").swiperight(function () {
                $(this).carousel('next');
            });
            $("#myCarouselBottom").swipeleft(function () {
                $(this).carousel('prev');
            });

            $(".points")
                .mouseenter(function () {
                    $(this).find("span").text("\\").css("color", "black");
                })
                .mouseleave(function () {
                    $(this).find("span").text("/").css("color", "#319fd4");
                });

            $(".slide-points")
                .mouseenter(function () {
                    $(this).find("span").text("\\").css("color", "#d89e14");
                })
                .mouseleave(function () {
                    $(this).find("span").text("/").css("color", "#fff");
                });

            $(".visit-points")
                .mouseenter(function () {
                    $(this).find("span").text("\\").css("color", "#d89e14");
                })
                .mouseleave(function () {
                    $(this).find("span").text("/").css("color", "#36a9e0");
                });

            $(".left")
                .mouseenter(function () {
                    $(this).find("span").css("display", "inline-block");
                })
                .mouseleave(function () {
                    $(this).find("span").css("display", "none");
                });
            $(".right")
                .mouseenter(function () {
                    $(this).find("span").css("display", "inline-block");
                })
                .mouseleave(function () {
                    $(this).find("span").css("display", "none");
                });

            $("#list2").on({
                mouseover: function () {
                    $("#image2").stop().show(1000);
                    $("#image1, #image3, #image4, #image5, #image6").stop().hide(1000);
                },

                mouseout: function () {
                    $("#image2").stop().hide(1000);
                }
            })

            $("#list1").on({
                mouseover: function () {
                    $("#image1").stop().show(1000);
                    $("#image2, #image3, #image4, #image5, #image6").stop().hide(1000);
                },

                mouseout: function () {
                    $("#image1").stop().hide(1000);
                }
            })


            $("#list3").on({
                mouseover: function () {
                    $("#image3").stop().show(1000);
                    $("#image1, #image2, #image4, #image5, #image6").stop().hide(1000);
                },

                mouseout: function () {
                    $("#image3").stop().hide(1000);
                }
            })





            $("#list4").on({
                mouseover: function () {
                    $("#image4").stop().show(1000);
                    $("#image1, #image3, #image2, #image5, #image6").stop().hide(1000);
                },

                mouseout: function () {
                    $("#image4").stop().hide(1000);
                }
            })


            $("#list5").on({
                mouseover: function () {
                    $("#image5").stop().show(1000);
                    $("#image1, #image3, #image4, #image2, #image6").stop().hide(1000);
                },

                mouseout: function () {
                    $("#image5").stop().hide(1000);
                }
            })



            $("#list6").on({
                mouseover: function () {
                    $("#image6").stop().show(1000);
                    $("#image1, #image3, #image4, #image5, #image2").stop().hide(1000);
                },

                mouseout: function () {
                    $("#image6").stop().hide(1000);
                }
            })

        // });

    });
}(jQuery));



