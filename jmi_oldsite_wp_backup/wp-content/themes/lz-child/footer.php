<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package lz Team
 */
?>
 <footer class="row jmi-contact">
                <div class="col-sm-12 jmi-groups">
                    <div class="row pt-5 upper-footer">
                    	<?php dynamic_sidebar( 'footer-contact' ); ?>
                    </div>
						<?php wp_nav_menu( array(
							'theme_location' => 'footer',
							'container'       => '',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'row pt-5 pb-5 lower-footer'

						) ); 
						?>
                    <div class="row tc">
                        <div class="col-sm-9 footer-end float-right">
							<?php wp_nav_menu( array(
								'theme_location' => 'footer_bottom',
								'menu_class' => 'nav-utility float-right',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'after'  => '<span class="glyphicon glyphicon-menu-right" style="font-size: 7px;"></span>'
							) ); ?>
                        </div>
                        <div class="col-sm-3 footer-select float-right">
                        	 <!-- GTranslate: https://gtranslate.io/ -->
								 <select onchange="doGTranslate(this);" class="notranslate goog-te-combo lang" id="gtranslate_selector">
								 	<option value="">Select Language</option>
								 	<option value="en|en">English</option>
								 	<option value="en|zh-CN">Chinese (Simplified)</option>
								 	<option value="en|hmn">Hmong</option>
								 </select>
								<div id="google_translate_element2"></div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <?php wp_footer(); ?>

<script type="text/javascript">




(function($) {

	        $("#history-image").on({
            mouseenter: function () {
                console.log("Dffdfd");
                var jmi_explore_image = "<?php  echo get_field('jmi_explore_image')['url']; ?>";
                var jmi_hover_image = "<?php  echo get_field('jmi_explore_hover_image')['url']; ?>";
                console.log(jmi_hover_image);
                $(this).css("background-image", "url(<?php  echo get_field('jmi_explore_hover_image')['url']; ?>)");
            },
            mouseleave: function () {
                $(this).css("background-image", "url(<?php  echo get_field('jmi_explore_image')['url']; ?>)");
            }
        });

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 2,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		map = new_map( $(this) );

	});

});

})(jQuery);
</script>


    </body>
</html>