<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//heapler function
if(!function_exists('pr')){
	function pr($txt){
		echo "<pre>";
		print_r($txt);

		return;
	}
}

// include 'inc/helper.php';
include 'inc/custom_nav.php';

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );

    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css',99 );


// END ENQUEUE PARENT ACTION

// // BEGIN ENQUEUE child ACTION
function child_thm_enqueue(){

	wp_enqueue_style('bootstrapcdn4', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
	wp_enqueue_style('bootstrapcdn7', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
	wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_enqueue_style('w3css', 'https://www.w3schools.com/w3css/4/w3.css' );


	wp_enqueue_style('custom-theme', get_stylesheet_directory_uri(). '/assets/css/style.css');

	wp_enqueue_script('mobile', 'http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js');
	wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyC06gnK3AC51ipkMFbItNOJyw0WyeYTz9E');
    wp_enqueue_script('googleTranslate', '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2');
 	
    wp_enqueue_script('coolcarousels', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
	wp_enqueue_script('custom_lz', get_stylesheet_directory_uri() .'/assets/js/custom.js');
}
add_action('wp_enqueue_scripts', 'child_thm_enqueue', 99);


// remove parent theme menu's
add_action( 'admin_menu', 'lz_adjust_the_wp_menu', 999 );
function lz_adjust_the_wp_menu() {
	$page = remove_submenu_page( 'themes.php', 'skt_blendit_guide' );
}

// remove parent theme custom stting's
add_action( "customize_register", "lz_theme_customize_register", 20 );
function lz_theme_customize_register( $wp_customize ) {

  $wp_customize->remove_section( 'slider_section' );
  $wp_customize->remove_section( 'section_second' );
}



// Regsiter footer bottm menu
add_action( 'after_setup_theme', 'theme_primary_menu', 20 );
function theme_primary_menu() {
	register_nav_menus( array(
		 'primary' => __( 'Primary Menu', 'lz' ),
		 'footer' => __( 'Footer Menu', 'lz' ),
		 'footer_bottom' => __( 'Footer Bottom Menu', 'lz' ),

	) );
}



// add ancher tag's class
add_filter('wp_nav_menu','add_menuclass');
function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}

// Regsiter widge area for header
function lz_widgets_init() { 	
	
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'lz' ),
		'description'   => __( 'Appears on blog page sidebar', 'lz' ),
		'id'            => 'sidebar-1',
		'before_widget' => '',		
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Header Widget', 'lz' ),
		'description'   => __( 'Appears on top of the header', 'lz' ),
		'id'            => 'header-widget',
		'before_widget' => '',		
		'before_title'  => '<h3 class="header-title" style="display:none;">',
		'after_title'   => '</h3><aside id="%1$s" class="headwidget %2$s">',
		'after_widget'  => '</aside>',
	) );

	register_sidebar(array(
     'name' => 'Header language',
     'id' => 'header-language',
      'description' => "",
       'before_widget' => '<div id="%1$s" class="widget text-right">',
       'after_widget' => '</div>',
       'before_title' => '<h3 class="group"><span>',
       'after_title' => '</span></h3>'
   ));

	register_sidebar(array(
     'name' => 'Footer Contact',
     'id' => 'footer-contact',
      'description' => "",
       'before_widget' => '<div id="%1$s" class="widget %2$s">',
       'after_widget' => '</div>',
       'before_title' => '<h3 class="group"><span>',
       'after_title' => '</span></h3>'
   ));

	
}
add_action( 'widgets_init', 'lz_widgets_init' );

// change default search form
function lz_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text" for="s">' . __( 'Search for:' ) . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <button id="searchsubmit"><i class="fa fa-search"></i></button>

    </div>
    </form>';

    return $form;
}

add_filter( 'get_search_form', 'lz_search_form', 100 );


// display nav menu descriptions
function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<p class="menu-item-description">' . $item->description . '</p>' . $args->link_after . '</a>', $item_output );
    }
 
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );

//add filter for acf map field call
function my_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyC06gnK3AC51ipkMFbItNOJyw0WyeYTz9E';
  return $api;
} 
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


