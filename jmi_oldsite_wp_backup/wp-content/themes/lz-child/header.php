<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package lz Team
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div class="container-fluid">
      <header>
          <div class="row">
              <div class="container">
                  <div class="upper-header">
                    <?php dynamic_sidebar( 'header-language' ); ?>
                     <?php //dynamic_sidebar( 'header-widget' ); ?>
                  </div>
                  <div class="row jmi-header">
                      <div class="col-xs-3 col-md-2 p-0 align-self-center">
                        <?php skt_blendit_the_custom_logo(); ?> 
                      </div>
                      <div class="col-xs-9 col-md-10 p-0">
                          <nav class="navbar navbar-expand-lg navbar-light bg-light p-0 mb-0" style="background-color: transparent!important">
                              <a class="navbar-brand" href="<?php echo site_url();?>">
                                  <h2 class="text-center m-0"><?php bloginfo('name'); ?></h2>
                                  <p><?php bloginfo('description'); ?></p>
                              </a>
                              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                  <span class="navbar-toggler-icon"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                   width="24" height="24"
                                   viewBox="0 0 24 24"
                                   style="fill:#fff;"><g id="surface1"><path style=" " d="M 0 2 L 0 4 L 24 4 L 24 2 Z M 0 11 L 0 13 L 24 13 L 24 11 Z M 0 20 L 0 22 L 24 22 L 24 20 Z "></path></g></svg></span>
                              </button>
                              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                 <?php wp_nav_menu( array(
                                  'theme_location' => 'primary', 
                                  'menu_class' => 'navbar-nav',
                                   'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'walker' => new wp_bootstrap_navwalker
                                ) ); ?>  
                              </div>
                          </nav>
                      </div>
                      <hr class="hidden-xs">
                  </div>
              </div>
          </div>
      </header>