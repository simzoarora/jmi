<?php
/**
 * Template Name: Home Page
 * The template for displaying Home Page.
 *
 *
 * @package lz Team
 */


get_header(); ?>
<?php if ( is_front_page() || is_home() ) { ?>
            <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:100%;">
                    <!-- Indicators -->
                           <?php $text = get_field('banner_text'); ?>
                             <?php
                                  $args = array(
                                      'post_type' => 'our_product',
                                      'post_status' => 'publish',
                                      'posts_per_page' => -1
                                  );

                                  $loop = new WP_Query( $args );
                                  if ( $loop -> have_posts() ) { 

                                     $count = count($loop->posts);
                                ?>
                                <div class="container indicators-container">   
                                    <!-- <a href="javascript:void(0);" class="round-button-top play-pause-button hidden-xs"><i id="top-play-button" class="fa fa-pause fa-2x"></i></a> -->
                                     <div class="icon hidden-xs">
                                      <svg viewBox="0 0 60 60">
                                        <title>play icon</title>
                                        <g>
                                          <path class="play hidden" fill="#f1f1f1" d="M24.89,40.84c-0.37,0.22-0.83,0.23-1.2,0.02s-0.6-0.61-0.6-1.04V20.2c0-0.43,0.23-0.83,0.6-1.04
                                      c0.37-0.21,0.83-0.21,1.2,0.02l16.35,9.81c0.36,0.21,0.58,0.6,0.58,1.02s-0.22,0.81-0.58,1.02L24.89,40.84z" />
                                          <path class="pause" fill="#f1f1f1" d="M28.03,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12c-0.86,0-1.56-0.7-1.56-1.56V19.06
                                      c0-0.86,0.7-1.56,1.56-1.56h3.12C27.33,17.5,28.03,18.2,28.03,19.06z M38.46,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12
                                      c-0.86,0-1.56-0.7-1.56-1.56V19.06c0-0.86,0.7-1.56,1.56-1.56h3.12C37.76,17.5,38.46,18.2,38.46,19.06z" />
                                          <path class="stroke-bg" fill="none" stroke="#999" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
                                    c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
                                          <path class="stroke" fill="none" stroke="#f1f1f1" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
                                    c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
                                        </g>
                                      </svg>
                                      </div>
                                    <!-- <div class="progressbar"></div> -->
                                    <ol class="carousel-indicators banner-slider mb-0">
                                        <?php
                                        for ($i=0; $i <$count ; $i++) { ?>
                                            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0) echo 'active';?>"></li>
                                        <?php } 
                                        ?>
                                    </ol>
                                  </div>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                  <?php $i = 1;
                                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <div class="item <?php if ($i == 1) echo 'active'; ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Los Angeles" style="width:100%;">
                                        <div class="carousel-caption container">
                                          <p>
                                          <span class="caption-top-1">Our Product /</span>
                                          <span class="caption-top-2"> <?php echo the_title();?></span>
                                        </p>
                                           
                                            <h1 class="pl-5 mb-4 mt-4"><?php echo $text; ?></h1>
                                            <ul class="slider-content pl-0">
                                                <p class="mb-4">Explore</p>
                                                <?php         
                                                $tags =  wp_get_post_tags( $post->ID);
                                                 $tagcount = count( $tags );
                                                       
                                                for ( $i = 0; $i < $tagcount; $i++ ) {
                                               echo ' <li><a href="#" class="slide-points"><span>/</span>'.$tags[$i]->name.'</a></li>';
                                                    
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                 <?php  $i++;  endwhile; ?>
                                </div>
                              <?php    }
                                  wp_reset_query();
                          ?>
                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-menu-left" style="display: none"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-menu-right" style="display: none"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="row visit-plan">
                <div class="container">
                    <a class="plan-button button button-table large" href="#">
                        <span class="first-icon-cell pr-2 pl-3">
                            <i class="fa fa-clipboard"></i>
                        </span>
                        <span class="text-cell pr-4 mr-3"><?php echo get_field('get_quote'); ?></span>
                        <span class="last-icon-cell pr-3">
                            <i class="glyphicon glyphicon-menu-right"></i>
                        </span>
                    </a>
                </div>
            </div>
            <!-- about jmi section -->
            <?php
                  $value_icon = get_field('value_icon');
                  $value_desc = get_field('value_description');
                  $jmi_explore_image = get_field('jmi_explore_image');
                  $jmi_explore_hover_image = get_field('jmi_explore_hover_image');
                  $jmi_explore_text = get_field('jmi_explore_text');
            ?>
            <div class="row about-jmi">
            <div class="col-sm-1"></div>
                <div class="col-sm-5 inner-content">
                    <div class="row jmi-values">
                        <div class="col-sm-3 text-right">
                            <img src="<?php echo $value_icon['url']; ?>" alt="abc" width="70px">
                        </div>
                        <div class="col-sm-9 pt-2 jmi-content">
                          <?php echo $value_desc; ?>
                            <ul class="explore pl-0">
                                <p class="mb-3">Explore</p>

                                  <?php    // check if the repeater field has rows of data
                            if( have_rows('jmi_explore_links') ):
                              // loop through the rows of data
                                while ( have_rows('jmi_explore_links') ) : the_row();
                                    // display a sub field value
                                    $text = get_sub_field('explore_text'); 
                                   echo '<li><a href="#" class="points"><span style="color: rgb(49, 159, 212);">/</span>'.$text.'</a></li>';  
                                endwhile;
                            else :
                                // no rows found
                            endif; ?>
                          
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs jmi-history history-one" id="history-image" 
                style="background-image: url(<?php echo $jmi_explore_image['url'];?>)">
                      <?php echo $jmi_explore_text; ?>
                    <div class="direction-link hidden-xs">
                        <span class="indicator"></span>
                        <a href="#" target="_blank">JMI Corporate Timeline</a>
                    </div>
                </div>
            </div>
      
              <!-------------------------------Second Carousel---------------------------------->

            <div class="row d-flex inner-slider">
                <div id="carousel-example" data-pause="false" class="hidden-xs professions-slider carousel background-slider flex-1 slide first-carousel" data-ride="carousel">
                   
                      <?php
                        $args = array(
                            'post_type' => 'our_professions',
                            'post_status' => 'publish',
                            'posts_per_page' => 4,
                        );

                        $loop = new WP_Query( $args );
                        if ( $loop -> have_posts() ) { ?>
                
                <ol class="hidden-xs carousel-indicators mb-0 ml-0 first-carousel-indicators">
                    <?php
                    for ($k=0; $k <$count ; $k++) { ?>
                    <li data-target="#carousel-example" class="<?php if ($k == 0) echo 'active'; ?>" data-slide-to="<?php echo $k; ?>"></li>
                    <?php } 
                    ?>
                    </ol>

                         <div class="carousel-inner">
                
                           <?php 
                          $j = 1;
                          $i = 1;
                            while ( $loop->have_posts() ) : $loop->the_post();  ?>
                          <?php
                            if ( 0 == $loop->current_post ):  ?>
                                 
                        <div class="item first-slide">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Second slide">
                        </div>  
                          <?php   elseif ( 1 == $loop->current_post ): ?>
                          <div class="item active first-slide">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Second slide">
                        </div>      
                           <?php  else: ?>
                                  <div class="item first-slide">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Second slide">
                        </div> 
                   
                        <?php   endif;
                         $i++;
                         endwhile;
                        }
                        wp_reset_query();
                      ?>
                    </div>
                </div>
                <div id="carousel-example-generic" data-pause="false" class="carousel professions-slider flex-1 slide second-carousel" data-ride="carousel">
                      <?php
                          $args = array(
                              'post_type' => 'our_professions',
                              'post_status' => 'publish',
                              'posts_per_page' => 4
                          );
                          $loop = new WP_Query( $args );
                          if ( $loop -> have_posts() ) {
                          $i = 1; 
                          $count = count($loop->posts);
                    ?>
                    <ol class="hidden-xs carousel-indicators mb-0 ml-0 second-carousel-slider">
                      <?php
                        for ($k=0; $k <$count ; $k++) { ?>
                          <li data-target="#carousel-example-generic" class="<?php if ($k == 0) echo 'active'; ?>" data-slide-to="<?php echo $k; ?>"></li>
                       <?php } 
                      ?>
                    </ol>
                    <!-- <a href="javascript:void(0);" class="round-button-second play-pause-button hidden-xs"><i id="second-play-button" class="fa fa-pause fa-2x"></i></a> -->
                    <div class="icon2 hidden-xs">
                            <svg viewBox="0 0 60 60">
                            <title>play icon</title>
                            <g>
                                <path class="play2 hidden2" fill="#f1f1f1" d="M24.89,40.84c-0.37,0.22-0.83,0.23-1.2,0.02s-0.6-0.61-0.6-1.04V20.2c0-0.43,0.23-0.83,0.6-1.04
                            c0.37-0.21,0.83-0.21,1.2,0.02l16.35,9.81c0.36,0.21,0.58,0.6,0.58,1.02s-0.22,0.81-0.58,1.02L24.89,40.84z" />
                                <path class="pause2" fill="#f1f1f1" d="M28.03,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12c-0.86,0-1.56-0.7-1.56-1.56V19.06
                            c0-0.86,0.7-1.56,1.56-1.56h3.12C27.33,17.5,28.03,18.2,28.03,19.06z M38.46,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12
                            c-0.86,0-1.56-0.7-1.56-1.56V19.06c0-0.86,0.7-1.56,1.56-1.56h3.12C37.76,17.5,38.46,18.2,38.46,19.06z" />
                                <path class="stroke-bg" fill="none" stroke="#999" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
                        c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
                                <path class="stroke2" fill="none" stroke="#f1f1f1" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
                        c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
                            </g>
                            </svg>
                    </div>

                    <div class="carousel-inner front-slider">
                      <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
                              <div class="item <?php if ($i == 1) echo 'active'; ?> second-slide">
                                  <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="first slide">
                                  <div class="card hidden-xs col-md-12">
                                  <p style="padding:1% 5%;">
                                    <span class="proxima-bold" >Our Professions  / </span>
                                    <span class="minion-italic"><?php echo the_title();?></span>
                                </p>
                                    <div class="w-50">
                                        <h5 class="mt-0 mb-0 float-left">
                                        "<?php echo get_the_excerpt(); ?>"</h5>
                                        <a href="<?php echo get_the_permalink();?>" class="more float-right">More</a>
                                    </div>
                                  </div>
                              </div>
                       <?php   $i++;
                     endwhile;
                    }
                    wp_reset_query();
                  ?>
                    </div>
                    <a class="left carousel-control left-arrow" id="second-left" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-menu-left" style="display: none"></span>
                    </a>
                    <a class="right carousel-control right-arrow" id="second-right" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-menu-right" style="display: none"></span>
                    </a>
                </div>
            </div>

            <!----------------------------------END-------------------------------------->
            <!-- our future -->
             <?php 
                $future_image = get_field('future_image');
                $future_desc = get_field('future_description');
            ?>
            <div class="row jmi-future pt-5">
                <div class="col-sm-5 future-img  hidden-xs w3-animate-left w3-animate-opacity" w3-animate-fading
                style="background-image: url(<?php echo $future_image['url'];?>)">
                </div>
                <div class="col-sm-7 future-content w3-animate-left w3-animate-opacity w3-animate-fading">
               <?php echo $future_desc; ?>
                </div>
            </div>
            <!-- member lisitng  -->
            <div class="row">
                <div class="col-sm-10 p-0">
                    <div id="myCarouselBottom" class="carousel slide vertical">
                        <ol class="carousel-indicators mb-0 testimonial-slide">
                          <?php   $count = count(get_field('member_lisitng')); 
                          for ($i=0; $i < $count; $i++) { ?>
                          <li data-target="#myCarouselBottom" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 1 ) echo 'active';?>"></li>
                         <?php }
                        ?>
                        </ol>
                        <div class="icon3 hidden-xs">
                            <svg viewBox="0 0 60 60">
                                    <title>play icon</title>
                                    <g>
                                        <path class="play3 hidden3" fill="#f1f1f1" d="M24.89,40.84c-0.37,0.22-0.83,0.23-1.2,0.02s-0.6-0.61-0.6-1.04V20.2c0-0.43,0.23-0.83,0.6-1.04
                                    c0.37-0.21,0.83-0.21,1.2,0.02l16.35,9.81c0.36,0.21,0.58,0.6,0.58,1.02s-0.22,0.81-0.58,1.02L24.89,40.84z" />
                                        <path class="pause3" fill="#f1f1f1" d="M28.03,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12c-0.86,0-1.56-0.7-1.56-1.56V19.06
                                    c0-0.86,0.7-1.56,1.56-1.56h3.12C27.33,17.5,28.03,18.2,28.03,19.06z M38.46,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12
                                    c-0.86,0-1.56-0.7-1.56-1.56V19.06c0-0.86,0.7-1.56,1.56-1.56h3.12C37.76,17.5,38.46,18.2,38.46,19.06z" />
                                        <path class="stroke-bg" fill="none" stroke="#999" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
                                c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
                                        <path class="stroke3" fill="none" stroke="#f1f1f1" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
                                c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
                                    </g>
                            </svg>
                        </div>
                        <div class="carousel-inner"  style="background-image: url(<?php echo get_field('member_background')['url'];?>)">
                           <?php if( have_rows('member_lisitng') ): 
                             $t = 1;
                            // loop through the rows of data
                            while ( have_rows('member_lisitng') ) : the_row(); 
                                $member_image = get_sub_field('member_image');  
                                $member_quot = get_sub_field('member_quot',  false, false);  
                                $member_name = get_sub_field('member_name',  false, false);  
                                $member_position = get_sub_field('member_position',  false, false);  
                                $company = get_sub_field('company',  false, false); 
                   
                            ?>
                            <div class="<?php if($t == 1 ) echo 'active' ?> testimonials item">
                                <img src="<?php echo $member_image['url'] ?>" style="opacity: 0;">
                                <div class="carousel-caption content p-0" style="text-align: center;">
                                      <div class="speech-bubble">
                                        <img src="<?php echo $member_image['url'] ?>" style="opacity: 1;">
                                      </div>
                                      <p class="fisrt-heading mb-0">"<?php echo $member_quot;?>."</p>
                                   
                                    <p class="second-heading text-center"><?php echo $member_name; ?>&nbsp;<?php echo $member_position; ?>&nbsp;<?php echo $company; ?></p>
                                </div>
                            </div>
                             <?php $t++; endwhile;
                        else :
                            // no rows found
                        endif;
                        ?>
               
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  visit uo place -->
            <?php 
              $visit_heading = get_field('visit_heading');
              $visit_sub_heading = get_field('visit_sub_heading');
              $visit_map = get_field('map');
            ?>
            <div class="row jmi-visit pt-5">

          

             <div class="col-12 col-sm-12  col-xs-12 col-md-12 col-lg-6  col-xl-5 visit-content">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <p class="visit">VISIT US</p>
                        <h2 class="pb-1 pr-5 mt-4"><?php echo $visit_heading; ?></h2>
                        <hr>
                        <ul class="visit-list">
                            <p class="pt-1 pr-5 info"><?php echo $visit_sub_heading; ?></p>
                                   <?php
                                if( have_rows('parts_listing') ):
                                // loop through the rows of data
                                    $links = 1;
                                  while ( have_rows('parts_listing') ) : the_row();
                                      // display a sub field value
                                    $parts_name = get_sub_field('parts_name');  
                                    $parts_address = get_sub_field('parts_address'); 
                                  echo '<li  id="list'.$links.'"><a href="#" class="visit-points"><span>/</span>'.$parts_name.'</a></li>';
                                  $links++;
                                  endwhile;
                              else :
                                  // no rows found
                              endif;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12  col-xs-12 col-md-12 col-lg-6  col-xl-5 pb-5 pt-5 visit-map hidden-xs">
               <div class="map_location">
			   <img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map1.png" id="div1">
			   <div class="inner_div" id="image1" style="display:none;">
			   <img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map2.png"></div>
			   <div class="inner_div" id="image2" style="display:none;"><img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map3.png"></div>
			  <div class="inner_div"  id="image3" style="display:none;"> <img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map4.png"></div>
			  <div class="inner_div"  id="image4" style="display:none;"> <img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map5.png"></div>
			  <div class="inner_div"  id="image5" style="display:none;"> <img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map6.png"></div>
			  <div class="inner_div"  id="image6" style="display:none;"> <img src="http://jmi.code.luminoguru.com/wp-content/uploads/2018/09/map7.png"></div>
			  
			   </div>
            </div>
                
                </div>
              
				
<style>
			
</style>
</div>
<?php } get_footer(); ?>
