<?php  
/**
 * SKT Blendit functions and definitions
 *
 * @package SKT Blendit
 */

// Set the word limit of post content 
function skt_blendit_content($limit) {
$content = explode(' ', get_the_excerpt(), $limit);
if (count($content)>=$limit) {
array_pop($content);
$content = implode(" ",$content).'...';
} else {
$content = implode(" ",$content);
}	
$content = preg_replace('/\[.+\]/','', $content);
$content = apply_filters('skt_blendit_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
return $content;
}


function new_excerpt_length($length) {
    return 80;
}
add_filter('excerpt_length', 'new_excerpt_length'); 

/**
 * Set the content width based on the theme's design and stylesheet.
 */

if ( ! function_exists( 'skt_blendit_setup' ) ) : 
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function skt_blendit_setup() {
	if ( ! isset( $content_width ) )
		$content_width = 640; /* pixels */

	load_theme_textdomain( 'skt-blendit', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support('woocommerce');
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'title-tag' );
	add_image_size( 'skt-blendit-logo', 350, 100 );
	add_theme_support( 'custom-logo', array( 'size' => 'skt-blendit-logo' ) );
	add_image_size('homepage-thumb',570,570,true);		
	register_nav_menus( array(
		// 'primary' => __( 'Primary Menu', 'skt-blendit' ),
		// 'footer' => __( 'Footer Menu', 'skt-blendit' )	
	) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'ffffff'
	) );
	add_editor_style( 'editor-style.css' );
} 
endif; // skt_blendit_setup
add_action( 'after_setup_theme', 'skt_blendit_setup' );


function skt_blendit_widgets_init() { 	
	
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'skt-blendit' ),
		'description'   => __( 'Appears on blog page sidebar', 'skt-blendit' ),
		'id'            => 'sidebar-1',
		'before_widget' => '',		
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Header Widget', 'skt-blendit' ),
		'description'   => __( 'Appears on top of the header', 'skt-blendit' ),
		'id'            => 'header-widget',
		'before_widget' => '',		
		'before_title'  => '<h3 class="header-title" style="display:none;">',
		'after_title'   => '</h3><aside id="%1$s" class="headwidget %2$s">',
		'after_widget'  => '</aside>',
	) );				
	
}
// add_action( 'widgets_init', 'skt_blendit_widgets_init' );


function skt_blendit_font_url(){
		$font_url = '';		
		
		/* Translators: If there are any character that are not
		* supported by Roboto Condensed, trsnalate this to off, do not
		* translate into your own language.
		*/
		$robotocondensed = _x('on','robotocondensed:on or off','skt-blendit');		
		
		
		/* Translators: If there has any character that are not supported 
		*  by Scada, translate this to off, do not translate
		*  into your own language.
		*/
		$scada = _x('on','Scada:on or off','skt-blendit');	
		
		if('off' !== $robotocondensed ){
			$font_family = array();
			
			if('off' !== $robotocondensed){
				$font_family[] = 'Roboto Condensed:300,400,600,700,800,900';
			}
					
						
			$query_args = array(
				'family'	=> urlencode(implode('|',$font_family)),
			);
			
			$font_url = add_query_arg($query_args,'//fonts.googleapis.com/css');
		}
		
	return $font_url;
	}


function skt_blendit_scripts() {
	wp_enqueue_style('skt-blendit-font', skt_blendit_font_url(), array());
	wp_enqueue_style( 'skt-blendit-basic-style', get_stylesheet_uri() );
	wp_enqueue_style( 'skt-blendit-editor-style', get_template_directory_uri()."/editor-style.css" );
	wp_enqueue_style( 'nivo-slider', get_template_directory_uri()."/css/nivo-slider.css" );
	wp_enqueue_style( 'skt-blendit-main-style', get_template_directory_uri()."/css/responsive.css" );		
	wp_enqueue_style( 'skt-blendit-base-style', get_template_directory_uri()."/css/style_base.css" );
	// wp_enqueue_script( 'jquery-nivo', get_template_directory_uri() . '/js/jquery.nivo.slider.js', array('jquery') );
	// wp_enqueue_script( 'skt-blendit-custom-js', get_template_directory_uri() . '/js/custom.js' );	
		

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'skt_blendit_scripts' );

function skt_blendit_ie_stylesheet(){
	global $wp_styles;
	
	/** Load our IE-only stylesheet for all versions of IE.
	*   <!--[if lt IE 9]> ... <![endif]-->
	*
	*  Note: It is also possible to just check and see if the $is_IE global in WordPress is set to true before
	*  calling the wp_enqueue_style() function. If you are trying to load a stylesheet for all browsers
	*  EXCEPT for IE, then you would HAVE to check the $is_IE global since WordPress doesn't have a way to
	*  properly handle non-IE conditional comments.
	*/
	wp_enqueue_style('skt_blendit_ie', get_template_directory_uri().'/css/ie.css', array('skt_blendit_style'));
	$wp_styles->add_data('skt_blendit_ie','conditional','IE');
	}
add_action('wp_enqueue_scripts','skt_blendit_ie_stylesheet');


define('SKT_CREDIT','http://www.sktthemes.net/product-category/free-wordpress-themes/','skt-blendit');
define('SKT_URL','http://www.sktthemes.net','skt-blendit');
define('SKT_PRO_THEME_URL','http://www.sktthemes.net/shop/blendit-one-page-wordpress-theme/','skt-blendit');
define('SKT_THEME_DOC','http://sktthemesdemo.net/documentation/skt-blendit-doc/','skt-blendit');
define('SKT_LIVE_DEMO','http://sktthemesdemo.net/blendit/','skt-blendit');
define('SKT_THEMES','http://www.sktthemes.net/themes/','skt-blendit');

if ( ! function_exists( 'skt_blendit_credit' ) ) {
function skt_blendit_credit(){
		return "<a href=".esc_url(SKT_CREDIT)." target='_blank' rel='nofollow'>SKT Blendit Theme</a>";
}
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template for about theme.
 */
require get_template_directory() . '/inc/about-themes.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

 if ( ! function_exists( 'skt_blendit_the_custom_logo' ) ) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since  SKT Blendit 1.0
 */
function skt_blendit_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;

// get slug by id
function skt_blendit_get_slug_by_id($id) {
	$post_data = get_post($id, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug; 
}