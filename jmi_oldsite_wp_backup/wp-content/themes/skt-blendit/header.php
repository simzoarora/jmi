<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package SKT Blendit
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="header <?php if ( !is_front_page() && ! is_home() ) { ?>innerheader<?php } ?>" <?php if( get_theme_mod( 'hide_slides' ) ) { echo 'style="position:relative"'; } ?>>
  <div class="container">
   <div class="logo">  
      <?php skt_blendit_the_custom_logo(); ?>   
      <a href="<?php echo home_url('/'); ?>">
		<h1><?php bloginfo('name'); ?></h1>
        <p><?php bloginfo('description'); ?></p>      
      </a>    
   </div><!-- logo -->
    
    
<div class="widget-right">
       <?php if ( ! dynamic_sidebar( 'header-widget' ) ) : ?>
       <?php endif; ?> 
       <div class="toggle">
          <a class="toggleMenu" href="#"><?php esc_attr_e('Menu','skt-blendit'); ?></a>
       </div> 
        <div class="sitenav">
          <?php wp_nav_menu( array('theme_location' => 'primary') ); ?>         
        </div><!-- .sitenav--> 
        <div class="clear"></div> 
</div><!--.widget-right-->
 <div class="clear"></div>         
</div> <!-- container -->
</div><!--.header -->
<?php if ( is_front_page() || is_home() ) { ?>
<?php if( get_theme_mod( 'hide_slides' ) == '') { ?>
<!-- Slider Section -->
<?php for($sld=7; $sld<10; $sld++) { ?>
	<?php if( get_theme_mod('page-setting'.$sld)) { ?>
     <?php $slidequery = new WP_query('page_id='.get_theme_mod('page-setting'.$sld,true)); ?>
		<?php while( $slidequery->have_posts() ) : $slidequery->the_post();
        $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
        $img_arr[] = $image;
        $id_arr[] = $post->ID;
        endwhile;
  	  }
    }
?>
<?php if(!empty($id_arr)){ ?>
<section id="home_slider">
  <div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
      <?php 
	$i=1;
	foreach($img_arr as $url){ ?>
     <a href="<?php the_permalink(); ?>"><img src="<?php echo $url; ?>" title="#slidecaption<?php echo $i; ?>" /></a>
      <?php $i++; }  ?>
    </div>
		<?php 
        $i=1;
        foreach($id_arr as $id){ 
        $title = get_the_title( $id ); 
        $post = get_post($id); 
        $content = apply_filters('the_content', substr(strip_tags($post->post_content), 0, 200)); 
        ?>
    <div id="slidecaption<?php echo $i; ?>" class="nivo-html-caption">
      <div class="slide_info">
        <h2><?php echo $title; ?></h2>
        <?php echo $content; ?>
        <a class="slideMore" href="<?php echo esc_url( get_permalink() ); ?>"><?php esc_attr_e('Learn More','skt-blendit'); ?></a>       
        
        <?php if ( get_theme_mod('purchase_link') !== "") { ?>
            <a class="slideMore purchasenow" target="_blank" href="<?php echo esc_url(get_theme_mod('purchase_link','#')); ?>">
				<?php esc_attr_e('Purchase Now','skt-blendit'); ?>
             </a> 
         <?php } ?>
        
           
        <div class="clear"></div>       
      </div>
    </div>
    <?php $i++; } ?>
  </div>
  <div class="clear"></div>
  
  <?php if ( ! dynamic_sidebar( 'header-contact-form' ) ) : ?>
  <?php endif; ?>
  
</section>
<?php } else { ?>
<section id="home_slider">
  <div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slides/slider1.jpg" alt="" title="#slidecaption1" /> <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slides/slider2.jpg" alt="" title="#slidecaption2" /> <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slides/slider3.jpg" alt="" title="#slidecaption3" /></div>
    <div id="slidecaption1" class="nivo-html-caption">
      <div class="slide_info">
        <h2>
          <?php esc_html_e('Delivering Right Strategies Which speaks itself', 'skt-blendit');?>
        </h2>
        <p><?php esc_html_e('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae est at dolor auctor faucibus. Aenean hendrerit lorem eget nisi vulputate, vitae fringilla ligula dignissim. Phasellus feugiat quam efficitur', 'skt-blendit');?></p>
        <a class="slideMore" href="#"><?php esc_attr_e('Learn More','skt-blendit'); ?></a>
        <a class="slideMore purchasenow" href="#"><?php esc_attr_e('Purchase Now','skt-blendit'); ?></a>     
      </div>
    </div>
    <div id="slidecaption2" class="nivo-html-caption">
      <div class="slide_info">
        <h2>
          <?php esc_html_e('Delivering Right Strategies Which speaks itself', 'skt-blendit');?>
        </h2>
        <p><?php esc_html_e('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae est at dolor auctor faucibus. Aenean hendrerit lorem eget nisi vulputate, vitae fringilla ligula dignissim. Phasellus feugiat quam efficitur', 'skt-blendit');?></p> 
       <a class="slideMore" href="#"><?php esc_attr_e('Learn More','skt-blendit'); ?></a>
        <a class="slideMore purchasenow" href="#"><?php esc_attr_e('Purchase Now','skt-blendit'); ?></a>       
      </div>
    </div>
    <div id="slidecaption3" class="nivo-html-caption">
      <div class="slide_info">
        <h2>
          <?php esc_html_e('Delivering Right Strategies Which speaks itself', 'skt-blendit');?>
        </h2>
        <p><?php esc_html_e('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae est at dolor auctor faucibus. Aenean hendrerit lorem eget nisi vulputate, vitae fringilla ligula dignissim. Phasellus feugiat quam efficitur', 'skt-blendit');?></p> 
        <a class="slideMore" href="#"><?php esc_attr_e('Learn More','skt-blendit'); ?></a>
        <a class="slideMore purchasenow" href="#"><?php esc_attr_e('Purchase Now','skt-blendit'); ?></a> 
      </div>
    </div>
  </div>
  <div class="clear"></div>    
</section><!-- Slider Section -->
<?php } } } ?>

<?php if ( is_front_page() || is_home() ) { ?>
<?php if( get_theme_mod( 'hide_pagefourboxes' ) == '') { 
$txt_arr = array( esc_attr__('Idea & Plan','skt-blendit'), esc_attr__('Design & Develop','skt-blendit'), esc_attr__('Test & launch','skt-blendit'));
?>
<section id="pagearea">
  <div class="container"> 
  
       <?php if( get_theme_mod('page-welcome')) { ?>
      <?php $queryvar = new WP_query('page_id='.get_theme_mod('page-welcome' ,true)); ?>
      <?php while( $queryvar->have_posts() ) : $queryvar->the_post();?>
     
      <div class="fourbox first_column">  
          <div class="pagebx-dec"> 
             <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h3>
		     <?php echo skt_blendit_content(40); ?>          
          </div>
      </div>
      <?php endwhile; } else { ?>  
      
       <div class="fourbox first_column">  
          <div class="pagebx-dec"> 
           <h3><a href="#"><?php esc_attr_e('Welcome to Blendit','skt-blendit'); ?></a></h3>
           <p>
        <?php esc_attr_e('Bulum iaculis lacinia est. Proin dictum elemntum velit. Fusce euismod consequat ante. Lorem ipsum dolor sit met consectetuer adipiscing elit. ellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. ','skt-blendit'); ?>
         </p> 
          </div>
         </div>     
      <?php } ?>
       
       
       
  
             
       <?php for($p=1; $p<4; $p++) { ?>
      <?php if( get_theme_mod('page-column'.$p,false)) { ?>
      <?php $queryxxx = new WP_query('page_id='.get_theme_mod('page-column'.$p,true)); ?>
      <?php while( $queryxxx->have_posts() ) : $queryxxx->the_post(); ?>
      <div class="fourbox <?php if($p % 3== 0) { echo "last_column"; } ?>">     	
		 <?php if( has_post_thumbnail() ) { ?>
			<div class="thumbbx"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail();?></a> </div>            
          <?php } ?> 
          <div class="pagebx-dec"> 
           <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h3>
		   <?php echo skt_blendit_content(15); ?>          
          </div>
      </div>
      <?php endwhile;
       wp_reset_query(); ?>
      <?php } else { ?>
      <div class="fourbox <?php if($p % 3 == 0) { echo "last_column"; } ?>">     
      <div class="thumbbx"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/services-icon<?php echo $p; ?>.jpg" alt="" /></a></div>
      <div class="pagebx-dec"> 
      <h3><a href="#"><?php echo $txt_arr[$p-1]; ?></a></h3>
        <p><?php esc_attr_e('Cras aliquet, tellus a dignissim aliquam, nibh erat vulputate justo, in posuere tortor mi a nisi. Quisque orci dolor.','skt-blendit'); ?></p> 
       
      </div>   
    </div>
  <?php }} ?> 
  <div class="clear"></div> 
  </div><!-- container -->
</section><!-- #pagearea -->
<?php } } ?>