/*-----------------------------------------------------------------------------------*/
/* Responsive WordPress Theme */
/*-----------------------------------------------------------------------------------*/

Theme Name      :   SKT Blendit
Theme URI       :   http://www.sktthemes.net/shop/free-onepage-wprdpress-theme/
Version         :   1.1
Tested up to    :   WP 4.5.2
Author          :   SKT Themes
Author URI      :   http://www.sktthemes.net/

license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

/*-----------------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*-----------------------------------------------------------------------------------*/

email       :   support@sktthemes.com

/*-----------------------------------------------------------------------------------*/
/* Theme Resources */
/*-----------------------------------------------------------------------------------*/

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.

2 -     jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com


3 - Fonts used in this theme are defined below
	Roboto: https://www.google.com/fonts/specimen/Roboto
	License: Apache License, version 2.0								http://www.apache.org/licenses/LICENSE-2.0.html



4 - 	Images used from Pixabay.
	Pixabay provides images under CC0 license							(https://creativecommons.org/about/cc0)

	Slides:
	https://pixabay.com/en/urban-people-crowd-citizens-438393/
	https://pixabay.com/en/bar-pub-restaurant-drink-people-401546/
	https://pixabay.com/en/entrepreneur-startup-start-up-man-593361/
	
		

For any help you can mail us at support[at]sktthemes.com