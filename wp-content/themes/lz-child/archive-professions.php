<?php
/**
 *  Template Name: Professions Archives
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package lz Team
 */

get_header(); ?>
<div class="row collapse text-light feature-footer-active">
    <?php
    $global_nav =  get_pages(); 
    foreach ($global_nav as $key => $nav) {
        if( $nav->post_name == 'professions'){ 
            echo '<img src="'.get_the_post_thumbnail_url($nav->ID).'" alt="" class="lazyload"/>';
        }
    }
    ?> 
</div>
<div class="container">
      <div class="page_content">
    		 <section class="site-main">               
            		<?php if( have_posts() ) :
							while( have_posts() ) : the_post(); ?>
                            	<h3 class="entry-title"><?php the_title(); ?></h3>
                             
                      		<?php endwhile; else : endif; ?>
                    <?php  wp_reset_query(); ?>
            </section><!-- section-->
   
     
    <div class="clear"></div>
    </div><!-- .page_content --> 
 </div><!-- .container --> 
<?php get_footer(); ?>