<?php
/**
 * Template Name: Home Page
 * The template for displaying Home Page.
 *
 *
 * @package lz Team
 */


get_header(); ?>
<?php if ( is_front_page() || is_home() ) { ?>

  <?php $text = get_field('banner_text'); ?>
  <div id="feature" class="row collapse text-light feature-footer-active">
    <div class="inner-wrap medium-24 columns">
      <a href="#skip-c4811" class="skip-carousel-link">Skip carousel</a>
      <div id="c4811" class="carousel-container">
        <div class="arrows" id="arrows-4811">
          <div class="arrow pause" id="pause-4811"  data-cycle-cmd="pause" data-cycle-context="#slideshow-4811">
            <div class="invisible-button"></div>
            <a href="#" aria-label="pause" role="button">
              <!-- <object id="pause-icon-4811" type="image/svg+xml" data="/typo3conf/ext/msi_contentelements/Resources/Public/Icons/cycle2/pause-icon.svg"></object> -->
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="pause-icon-4811" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                <style type="text/css">
                .st0{fill:#F2F2F2;}
                .st1{opacity:0.3;fill:none;stroke:#FFFFFF;stroke-miterlimit:10;enable-background:new;}
                .st2{fill:none;stroke:#2AA9E0;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:2000,2000;stroke-dashoffset:2000;}
              </style>
              <rect x="20.3" y="20.3" class="st0" width="3.4" height="12.3"/>
              <rect x="28.3" y="20.5" class="st0" width="3.4" height="12.3"/>
              <circle id="circle" class="st1" cx="26" cy="26" r="23.5"/>
              <path id="progress-outer-circle-4811" class="st2" d="M26,4c12.1,0,22,9.9,22,22s-9.8,21.9-22,21.9S4,38,4,25.9S13.9,4,26,4"/>
            </svg>
          </a>
        </div>
        <div class="arrow resume" id="resume-4811"  data-cycle-cmd="resume" data-cycle-context="#slideshow-4811" style="display: none;">
          <div class="invisible-button"></div>
          <a href="#" aria-label="resume" role="button">
            <!-- <object id="resume-icon-4811" type="image/svg+xml" data="/typo3conf/ext/msi_contentelements/Resources/Public/Icons/cycle2/resume-icon.svg"></object> -->
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
            <style type="text/css">
            .st0{fill:#F2F2F2;}
            .st1{opacity:0.3;fill:none;stroke:#FFFFFF;stroke-miterlimit:10;enable-background:new    ;}
          </style>
          <polygon class="st0" points="34.6,25.9 21.6,33.9 21.6,18.1 "/>
          <circle id="circle" class="st1" cx="26" cy="26" r="23.5"/>
        </svg>
      </a>
    </div>
    <div id="cycle-pager--left-4811" class="cycle-pager cycle-pager-4811 cycle-pager--left"></div>
  </div>
  <div id="slideshow-4811" data-persona-container class="carousel top-slider"
  data-cycle-auto-init=false
  data-cycle-slides="> div.slide"
  data-cycle-paused=true
  data-cycle-swipe=true
  data-cycle-swipe-fx=scrollHorz
  data-cycle-prev=".cycle-prev"
  data-cycle-next=".cycle-next"
  data-cycle-timeout="8000"
  data-cycle-speed="1000"
  data-cycle-center-horz=true
  data-cycle-fx=scrollHorzRight
  data-cycle-easing="easeInOutQuad"
  data-cycle-pager=".cycle-pager-4811"
  data-cycle-pager-template="<a href='#'>•</a>"
  data-cycle-auto-height="calc"
  data-cycle-log="false">

  <div class="cycle-prev">
    <i class="fa fa-angle-left" aria-hidden="true"></i>
  </div>
  <div class="cycle-next">
    <i class="fa fa-angle-right" aria-hidden="true"></i>
  </div>

  <?php $text = get_field('banner_text'); ?>
  <?php
  $args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => -1
  );

  $loop = new WP_Query( $args );
  if ( $loop -> have_posts() ) { 

   $count = count($loop->posts);
   ?>
   <?php $i = 1;
   while ( $loop->have_posts() ) : $loop->the_post(); 
    $color1 = get_field('background_linear_color_first', $post->ID);
    $color2 = get_field('background_linear_color_second', $post->ID);
    ?>
    <div data-personas-element class="slide <?php if ($i == 1) echo 'first'; ?>">
      <div id="c20045" class="row collapse " data-theme-class="bh-color__theme-c20045" data-hide-for-personas="" data-top-for-personas="">
        <div class="columns">
          <section class="big-header bh__height-size-3 text-light">
            <div class="bh__background bh__background-color " style="background-image: linear-gradient(<?php echo $color1; ?>, <?php echo $color2; ?>);">
              <div class="bh__gradient" style="background-image: linear-gradient(to top,  0%,  40%);"></div>
              <div class="bh__background-image blur-up lazyload bh__height-size-3 bh__bg-image-top-center " 
              style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);background-position: center;background-size: cover;" data-sizes="auto">
            </div>
          </div>
          <div class="bh__content-wrap">
            <div class="bh__content top-content">
              <div class="bh__content-inner-wrap">
                <h3 class="bh__subtitle"><a href="#">
                  <span class="line-wrap">
                    <span class="line top-line">
                      <span class="bh__info">Our Product / </span>
                      <?php echo the_title();?>
                    </span>
                  </span>
                </a>
              </h3>
              <h2 class="bh__title ">
                <a href="#">
                  <span class="line-wrap">
                    <span class="line quote"><?php echo get_the_excerpt(); ?></span>
                  </span>
                </a>
              </h2>
              <ul class="bh__links bh__link-underline">
                <li><a href="#">Explore</a></li>
              </ul>
              <ul class="bh__links bh__link-type">
                <?php  $tags =  wp_get_post_tags( $post->ID);
                $tagcount = count( $tags );
                for ( $i = 0; $i < $tagcount; $i++ ) {
                  echo ' <li><a href="#" class="slide-points"><span>/</span> '.$tags[$i]->name.' </a></li>';                                              }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </section><!-- end .feature-ce -->
      </div><!-- end .columns -->
    </div><!-- end .row -->
  </div>

  <?php  $i++;  endwhile; ?>
</div>
<?php    }
wp_reset_query();
?>
</div>

<div id="cycle-pager-4811" class="cycle-pager cycle-pager-4811 cycle-pager--right"></div>
</div>
<a name="skip-c4811"></a>
<div class="feature-footer">
  <div class="inner-wrap">
    <a class="plan-button button button-table large" href="contact-us">
      <span class="first-icon-cell get-first">
        <i class="fa fa-clipboard" aria-hidden="true"></i>
      </span>
      <span class="text-cell text"><?php echo get_field('get_quote'); ?></span>
      <span class="last-icon-cell get-last">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
      </span>
    </a>
  </div><!-- end .inner-wrap -->
</div><!-- end .feature-footer -->
</div><!-- end .columns -->
</div><!-- end #feature -->

<div id="content-wrap" class="row">
  <a id="skip-feature"></a>
  <div class="medium-24 columns">
    
    <?php
    $value_icon = get_field('value_icon');
    $jmi_value_text = get_field('jmi_value_text');
    $jmi_value_title = get_field('jmi_value_title');
    $jmi_value_description = get_field('jmi_value_description');
    $jmi_explore_image = get_field('jmi_explore_image');
    $jmi_explore_hover_image = get_field('jmi_explore_hover_image');
    
    $jmi_history_title = get_field('jmi_history_title',false, false);
    $jmi_history_text = get_field('jmi_history_text',false, false);
    $jmi_history_link = get_field('jmi_history_link',false, false);
    $jmi_history_timeline = get_field('jmi_history_timeline');

                                // pr($jmi_explore_image);
    ?>
    <main id="content-block-1" class="row collapse">
      <div class="medium-24 columns">
        <div class="layout-container lc__home-intro">
          <div class="lc__inner-wrap">
            <div class="lc__background  "></div>
            <div class="lc__background-2 history-image" style="background-image: url(<?php echo $jmi_explore_image['url'] ?>)">


            </div>
            <div class="row collapse lc__content  ">
              <div class="columns ">
                <div class="lc__background-3 history-image" ></div>
                <div class="lc__sidebar text-light">
                  <div id="c6173" class="history-content">
                    <h3><?php echo $jmi_history_title; ?>
                    
                  </h3>
                  <a href="#" target="_blank" class="address">
                   <span class="fac"><?php echo $jmi_history_text; ?></span>
                   <span class="old">
                    <?php
                                                            // check if the repeater field has rows of data
                    if( have_rows('jmi_history_link') ):

                                                              // loop through the rows of data
                      while ( have_rows('jmi_history_link') ) : the_row();

                                                                    // display a sub field value
                        echo the_sub_field('history_link'); 
                        echo "<br>";
                      endwhile;

                    else :
                                                                // no rows found
                    endif;
                    ?>
                  </span>
                </a>
                <div class="direction-link">
                  <span class="indicator"></span>
                  <a href="#" target="_blank">
                    <?php echo $jmi_history_timeline; ?>
                  </a>
                </div>

              </div>
            </div>
            <div class="lc__intro-content">
              <div class="curiosity-badge">
                <div class="curiosity-badge__icon-wrap">
                  <img src="<?php echo $value_icon['url']; ?>" class="curiosity-badge__icon" alt="Long Live Curiosity">
                </div>
              </div>
              <section id="c13039" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-center tai__image-above   ">
                <div class="ce-content rte-content rte-text-medium ">

                  <div class="headline-group  ">
                   <div class="subtitle"> 
                    <?php echo $jmi_value_text; ?>
                  </div>
                  <h2 class="h1-match title"> 
                    <?php echo $jmi_value_title; ?>
                  </h2>
                </div>
                <p> 
                  <?php echo $jmi_value_description; ?>
                </p>

                <p><a href="#" target="_self" class="internal-link arrow-link internal-explore">Explore</a></p>
                
                <ul class="internal-explore-type">
                                                              <?php    // check if the repeater field has rows of data
                                                              if( have_rows('jmi_explore_links') ):
                                                                      // loop through the rows of data
                                                                while ( have_rows('jmi_explore_links') ) : the_row();
                                                                            // display a sub field value
                                                                  $postobject = get_sub_field('explore_text'); 
                                                                  
                                                                  echo '<li><a href="'.get_permalink($postobject->ID).'" class="explore-points">
                                                                  <span class="slash-animation" style="color: rgb(49, 159, 212);">/</span> '.$postobject->post_title.' </a></li>';  
                                                                endwhile;
                                                              else :
                                                                        // no rows found
                                                              endif; ?>
                                                            </ul>
                                                          </div>
                                                        </section>
                                                      </div>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <!-- second slider start -->
                                              <div id="c4817" class="layout-container     lc__top-offset-25   lc__stack-layer-0    " style=""  >
                                                <div class="lc__background-2  height-0"></div>
                                                <div class="lc__background   "   ></div>
                                                <div class="lc__inner-wrap lc__pad-top-05 lc__pad-bottom-10 lc__mobile-bleed-top  lc__mobile-bleed-sides" style="padding-top: 8.125rem">
                                                  <div class="row collapse lc__content lc__wide ">
                                                    <div class="columns lc__content-offset-top-0 lc__content-bottom-up-0">
                                                      <div class="lc__content-inner-wrap professions-slider">
                                                        <a href="#skip-c17856" class="skip-carousel-link">Skip carousel</a>
                                                        <div id="c17856" class="carousel-container structured-carousel ">
                                                          <div id="slideshow-17856-small" data-persona-container class="cycle-slideshow cycle-slideshow-small" aria-hidden="true"
                                                          data-cycle-auto-init=false
                                                          data-cycle-starting-slide=0
                                                          data-cycle-paused=true
                                                          data-cycle-prev=".cycle-prev"
                                                          data-cycle-next=".cycle-next"
                                                          data-cycle-timeout="8000"
                                                          data-cycle-speed="750"
                                                          data-cycle-center-horz=true
                                                          data-cycle-fx=scrollHorzRight
                                                          data-cycle-easing="easeInOutQuad"
                                                          data-cycle-log="false">
                                                          <div class="cycle-prev" style="display:none">
                                                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                          </div>
                                                          <div class="cycle-next" style="display:none">
                                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                          </div>
                                                          <?php
                                                          $args = array(
                                                            'post_type' => 'professions',
                                                            'post_status' => 'publish',
                                                            'posts_per_page' => 5,
                                                            'orderby'   => 'publish_date',

                                                          );

                                                          $loop = new WP_Query( $args );
                                                          if ( $loop -> have_posts() ) { ?>

                                                           <?php 
                                                           $j = 1;
                                                           $i = 1;
                                                           while ( $loop->have_posts() ) : $loop->the_post();  
                                                            
                                                            if ( 1 == $loop->current_post ): ?>
                                                              <img data-hide-for-personas="" data-top-for-personas="" 
                                                              class="background-image first" 
                                                              src="<?php echo get_the_post_thumbnail_url(); ?>" width="730" height="411" alt="" /> 
                                                              
                                                              <?php  else: ?>
                                                                <img data-hide-for-personas="" data-top-for-personas="" 
                                                                class="background-image" 
                                                                src="<?php echo get_the_post_thumbnail_url(); ?>" width="730" height="411" alt="" /> 
                                                              <?php endif;  
                                                            endwhile;
                                                          }
                                                          wp_reset_query();
                                                          ?>
                                                          <div class="carousel-overlay"></div>

                                                        </div>

                                                        <div class="arrows second-slide">
                                                          <div id="cycle-pager-17856" class="cycle-pager"></div>
                                                          <div class="arrow pause" id="pause-17856"  data-cycle-cmd="pause" data-cycle-context="#slideshow-17856">
                                                            <div class="invisible-button"></div>
                                                            <a href="#" aria-label="pause" role="button">
                                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="pause-icon-17856" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                                                <style type="text/css">
                                                                .st0{fill:#F2F2F2;}
                                                                .st1{opacity:0.3;fill:none;stroke:#FFFFFF;stroke-miterlimit:10;enable-background:new;}
                                                                .st2{fill:none;stroke:#2AA9E0;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:2000,2000;stroke-dashoffset:2000;}
                                                              </style>
                                                              <rect x="20.3" y="20.3" class="st0" width="3.4" height="12.3"/>
                                                              <rect x="28.3" y="20.5" class="st0" width="3.4" height="12.3"/>
                                                              <circle id="circle" class="st1" cx="26" cy="26" r="23.5"/>
                                                              <path id="progress-outer-circle-17856" class="st2" d="M26,4c12.1,0,22,9.9,22,22s-9.8,21.9-22,21.9S4,38,4,25.9S13.9,4,26,4"/>
                                                            </svg>
                                                          </a>
                                                        </div>
                                                        <div class="arrow resume" id="resume-17856"  data-cycle-cmd="resume" data-cycle-context="#slideshow-17856" style="display: none;">
                                                          <div class="invisible-button"></div>
                                                          <a href="#" aria-label="resume" role="button">
                                                            <!-- <object id="resume-icon-17856" type="image/svg+xml" data="/typo3conf/ext/msi_contentelements/Resources/Public/Icons/cycle2/resume-icon.svg"></object> -->
                                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                                            <style type="text/css">
                                                            .st0{fill:#F2F2F2;}
                                                            .st1{opacity:0.3;fill:none;stroke:#FFFFFF;stroke-miterlimit:10;enable-background:new    ;}
                                                          </style>
                                                          <polygon class="st0" points="34.6,25.9 21.6,33.9 21.6,18.1 "/>
                                                          <circle id="circle" class="st1" cx="26" cy="26" r="23.5"/>
                                                        </svg>
                                                      </a>
                                                    </div>
                                                  </div>

                                                  <div id="slideshow-17856" data-persona-container class="cycle-slideshow cycle-slideshow-backgroundimage" aria-hidden="true"
                                                  data-cycle-auto-init=false
                                                  data-cycle-paused=true
                                                  data-cycle-prev=".cycle-prev"
                                                  data-cycle-next=".cycle-next"
                                                  data-cycle-timeout="8000"
                                                  data-cycle-speed="750"
                                                  data-cycle-center-horz=true
                                                  data-cycle-fx=scrollHorzRight
                                                  data-cycle-easing="easeInOutQuad"
                                                  data-cycle-pager="#cycle-pager-17856"
                                                  data-cycle-pager-template="<a href='#'>•</a>"
                                                  data-cycle-log="false">
                                                  <div class="cycle-prev">
                                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                  </div>
                                                  <div class="cycle-next">
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                  </div>
                                                  <?php
                                                  $args = array(
                                                    'post_type' => 'professions',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => 5
                                                  );
                                                  $loop = new WP_Query( $args );
                                                  if ( $loop -> have_posts() ) {
                                                    $i = 1; 
                                                    $count = count($loop->posts);
                                                    ?>
                                                    <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>


                                                      <img data-hide-for-personas="" data-top-for-personas="" class="background-image <?php if ($i == 1) echo 'first'; ?>" src=<?php echo get_the_post_thumbnail_url(); ?>" alt="" />
                                                    <?php endwhile;}
                                                    wp_reset_query();
                                                    ?>
                                                  </div> 

                                                </div>
                                                <div class="carousel-background-block">
                                                  <div id="slideshow-17856-text" data-persona-container class="cycle-slideshow cycle-slideshow-text"
                                                  data-cycle-auto-init=false
                                                  data-cycle-slides="> div.slide"
                                                  data-cycle-paused=true
                                                  data-cycle-prev=".cycle-prev"
                                                  data-cycle-next=".cycle-next"
                                                  data-cycle-timeout="8000"
                                                  data-cycle-speed="750"
                                                  data-cycle-center-horz=true
                                                  data-cycle-fx="scrollHorzRight"
                                                  data-cycle-easing="easeInOutQuad"
                                                  data-cycle-pager="#cycle-pager-17856"
                                                  data-cycle-pager-template=""
                                                  data-cycle-log="false">
                                                  <div class="cycle-prev" style="display:none">
                                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                  </div>
                                                  <div class="cycle-next" style="display:none">
                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                  </div>
                                                  <?php
                                                  $args = array(
                                                    'post_type' => 'professions',
                                                    'post_status' => 'publish',
                                                    'posts_per_page' => 5
                                                  );
                                                  $loop = new WP_Query( $args );
                                                  if ( $loop -> have_posts() ) {
                                                    $tr = 1; 
                                                    $count = count($loop->posts);
                                                    ?>
                                                    <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                                      
                                                      <div class="slide  <?php if ($i == 1) echo 'first'; ?>" data-hide-for-personas="" data-top-for-personas="">
                                                        <section>
                                                          <div class="content-area text-light">
                                                            <div class="text">
                                                              <span class="line-wrap">
                                                                <span class="line">
                                                                  <p>Our Professions / <span class="electro"><?php echo the_title();?></span></p>
                                                                </span>
                                                              </span>
                                                              <span class="line-wrap line-quote">
                                                                <span class="line">
                                                                  <p class="quote-line"><?php echo get_the_excerpt(); ?></p>
                                                                </span>
                                                              </span>
                                                              <span class="line-wrap line-more">
                                                                <span class="line">
                                                                  <a href="<?php echo get_the_permalink();?>" class="more-slide">More</a>
                                                                </span>
                                                              </span>
                                                            </div>
                                                          </div>
                                                        </section>
                                                      </div>

                                                      <?php   $i++;
                                                    endwhile;
                                                  }
                                                  wp_reset_query();
                                                  ?> 
                                                </div>
                                              </div>
                                              <a name="skip-c17856"></a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <!--  second slider end here -->
                                    <?php 
                                    $future_image = get_field('future_image');
                                    
                                    $future_text = get_field('future_text');
                                    $future_title = get_field('future_title');
                                    $future_desc = get_field('future_desc',false,false);
                                    $future_link = get_field('future_link');
                                    ?>
                                    <div id="c4819" class="layout-container     lc__top-offset-0 robot-boy  lc__stack-layer-0 lc__bgimage-bottom-right lc__mobile-image-bottom  " style="" data-image-animation="bigSlideInLeft" data-content-animation="fadeInAndSlideInLeft">
                                      <div class="lc__background-2  height-default" ></div>
                                      <div class="lc__background lc__left-of-center  lazyload" data-sizes="auto"  style="background-image: url(<?php echo $future_image['url'];?>)"></div>
                                      <div class="lc__inner-wrap lc__pad-top-175 lc__pad-bottom-15 lc__mobile-bleed-top lc__mobile-bleed-bottom ">
                                        <div class="row collapse lc__content  narrow-right">
                                          <div class="columns lc__content-offset-top-0 lc__content-bottom-up-0">
                                            <div class="lc__content-inner-wrap">
                                              <section id="c4818" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-center tai__image-above   ">
                                                <div class="ce-content rte-content rte-text-small ">
                                                 
                                                  <div class="headline-group with-rule future-title">
                                                    <div class="subtitle future-subtitle"><?php echo $future_text;?></div>
                                                    <h2 class="h1-match title future-maintitle"><?php echo $future_title;?></h2>
                                                  </div>
                                                  
                                                  <p><?php echo $future_desc;?></p>
                                                  <p class="future_explore">
                                                    <a href="#" target="_self" class="internal-link arrow-link internal-explore">
                                                     <?php echo get_field('future_explore'); ?></a>
                                                   </p>
                                                   
                                                   <?php
                                  // check if the repeater field has rows of data
                                                   if( have_rows('future_link') ): ?>
                                                     <ul class="arrow-list future-links">

                                                       <?php while ( have_rows('future_link') ) : the_row();
                                          // display a sub field value
                                                        echo '<li><a class="internal-link future-link" href="#" target="_self">/ '.get_sub_field('future_link_text').'</a></li>';
                                                        
                                                      endwhile; ?>
                                                    </ul>
                                                  <?php else :
                                      // no rows found
                                                  endif;
                                                  ?>

                                                  
                                                  
                                                </div>
                                              </section>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div id="c4824" class="layout-container lc__bleed-left  lc__theme-color-1  lc__top-offset-0   lc__stack-layer-2    " style=""  >
                                      <div class="lc__background-2  height-0" style="background-image: url(<?php echo get_field('member_background')['url'];?>)"></div>
                                      <div class="lc__background   slide-background-img"   >
                                        <div class="grad"   ></div>
                                      </div>
                                      <div class="lc__inner-wrap lc__pad-top-10 lc__pad-bottom-10 lc__mobile-bleed-top lc__mobile-bleed-bottom ">
                                        <div class="row collapse lc__content  ">
                                          <div class="columns lc__content-offset-top-0 lc__content-bottom-up-0">
                                            <div class="lc__content-inner-wrap">
                                              <a href="#skip-c4823" class="skip-carousel-link">Skip carousel</a>
                                              <div id="c4823" class="carousel-container">
                                                <div class="arrows" id="arrows-4823">
                                                  <div class="arrow pause" id="pause-4823"  data-cycle-cmd="pause" data-cycle-context="#slideshow-4823">
                                                    <div class="invisible-button"></div>
                                                    <a href="#" aria-label="pause" role="button">
                                                      <!-- <object id="pause-icon-4823" type="image/svg+xml" data="/typo3conf/ext/msi_contentelements/Resources/Public/Icons/cycle2/pause-icon.svg"></object> -->
                                                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="pause-icon-4823" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                                        <style type="text/css">
                                                        .st0{fill:#F2F2F2;}
                                                        .st1{opacity:0.3;fill:none;stroke:#FFFFFF;stroke-miterlimit:10;enable-background:new;}
                                                        .st2{fill:none;stroke:#2AA9E0;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:2000,2000;stroke-dashoffset:2000;}
                                                      </style>
                                                      <rect x="20.3" y="20.3" class="st0" width="3.4" height="12.3"/>
                                                      <rect x="28.3" y="20.5" class="st0" width="3.4" height="12.3"/>
                                                      <circle id="circle" class="st1" cx="26" cy="26" r="23.5"/>
                                                      <path id="progress-outer-circle-4823" class="st2" d="M26,4c12.1,0,22,9.9,22,22s-9.8,21.9-22,21.9S4,38,4,25.9S13.9,4,26,4"/>
                                                    </svg>
                                                  </a>
                                                </div>
                                                <div class="arrow resume" id="resume-4823"  data-cycle-cmd="resume" data-cycle-context="#slideshow-4823" style="display: none;">
                                                  <div class="invisible-button"></div>
                                                  <a href="#" aria-label="resume" role="button">
                                                    <!-- <object id="resume-icon-4823" type="image/svg+xml" data="/typo3conf/ext/msi_contentelements/Resources/Public/Icons/cycle2/resume-icon.svg"></object> -->
                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                                    <style type="text/css">
                                                    .st0{fill:#F2F2F2;}
                                                    .st1{opacity:0.3;fill:none;stroke:#FFFFFF;stroke-miterlimit:10;enable-background:new    ;}
                                                  </style>
                                                  <polygon class="st0" points="34.6,25.9 21.6,33.9 21.6,18.1 "/>
                                                  <circle id="circle" class="st1" cx="26" cy="26" r="23.5"/>
                                                </svg>
                                              </a>
                                            </div>
                                            <div id="cycle-pager--left-4823" class="cycle-pager cycle-pager-4823 cycle-pager--left"></div>
                                          </div>
                                          <div id="slideshow-4823" data-persona-container class="carousel "
                                          data-cycle-auto-init=false
                                          data-cycle-slides="> div.slide"
                                          data-cycle-paused=true
                                          data-cycle-timeout="5000"
                                          data-cycle-speed="1000"
                                          data-cycle-center-horz=true
                                          data-cycle-fx=slideUp
                                          data-cycle-easing="easeInOutQuad"
                                          data-cycle-pager=".cycle-pager-4823"
                                          data-cycle-pager-template="<a href='#'>•</a>"
                                          data-cycle-auto-height="calc"
                                          data-cycle-log="false">

                                          <?php if( have_rows('member_lisitng') ): 
                                            
                                            $sliderlimit = count(get_field('member_lisitng'));
                                            $memberSlider = array();
                                            // loop through the rows of data
                                            while ( have_rows('member_lisitng') ) : the_row(); 
                                              
                                              $member_image = get_sub_field('member_image');  
                                              $member_quot = get_sub_field('member_quot',  false, false);  
                                              $member_name = get_sub_field('member_name',  false, false);  
                                              $member_position = get_sub_field('member_position',  false, false);  
                                              $company = get_sub_field('company',  false, false); 
                                              
                                              $memberSlider[] = array ( 
                                                'name' => $member_name,
                                                'quote' => $member_quot,
                                                'image' => $member_image,
                                                'postition' => $member_position,
                                                'company' => $company,
                                              );
                                            endwhile;

                                          endif;

                                          
                                          ?>


                                          <?php
                                          $industrySlider = array();
                                          $args = array(
                                            'post_type' => 'post',
                                            'post_status' => 'publish',
                                            'category_name'         => 'industry-news',
                                            'posts_per_page' => $sliderlimit,
                                            'orderby'=> 'publish_date', 'order' => 'ASC'
                                          );
                                          $loop = new WP_Query( $args ); 
                                          while ( $loop->have_posts() ) : $loop->the_post(); 
                                           $cats = get_the_category(get_the_ID());
                                           
                                           $catArr = [];
                                           foreach ($cats as $key => $cat) {
                                            $catArr[] =  $cat->name;
                                          }
                                          $industrySlider[] = array ( 
                                            'name' => get_the_title(),
                                            'quote' => get_the_excerpt(),
                                            'image' => '/wp-content/themes/lz-child/img/industry.png',
                                            'cat' => $catArr
                                          );


                                        endwhile;
                                        wp_reset_query();     

                                        ?>

                                        <?php
                                        $CorporateSlider = array();
                                        $args = array(
                                          'post_type' => 'post',
                                          'post_status' => 'publish',
                                          'category__in' => array(21,1),
                                          'posts_per_page' => $sliderlimit,
                                          'orderby'=> 'publish_date', 'order' => 'ASC'
                                        );
                                        $loop = new WP_Query( $args ); 

                                        while ( $loop->have_posts() ) : $loop->the_post(); 
                                          $cats = get_the_category(get_the_ID());
                                          
                                          $catArr = [];
                                          foreach ($cats as $key => $cat) {
                                            $catArr[] =  $cat->name;
                                          }
                                          $CorporateSlider[] = array ( 
                                            'name' => get_the_title(),
                                            'quote' => get_the_excerpt(),
                                            'image' => '/wp-content/themes/lz-child/img/corp.png',
                                            'cat' => $catArr
                                          );
                                        endwhile;
                                        wp_reset_query();            

                                   // $CorporateSlider, $industrySlider, $memberSlider
                                        $max = max([count($CorporateSlider), count($industrySlider), count($memberSlider)]);
                                        $slides = [];

                                        for( $i=0; $i<$max; $i++){
                                          $slides[$i][] = (isset($memberSlider[$i])) ? $memberSlider[$i] : null; 
                                          $slides[$i][] = (isset($industrySlider[$i])) ? $industrySlider[$i] : null;
                                          $slides[$i][] = (isset($CorporateSlider[$i])) ? $CorporateSlider[$i] : null;
                                        }
                                        ?>
                                        <?php foreach($slides as $slider ):  ?> 
                                          <?php foreach ($slider as $key => $value): $t = 1; 

                                            if( !empty($value) ):  ?>
                                             
                                              <div data-personas-element class="slide <?php if($t == 1 ) echo 'first' ?>">
                                                <blockquote id="c4821" class="feature-quote   ce-margin-top-0 ce-margin-bottom-0">
                                                 <?php if( !empty($value['image']['url']) ){
                                                   echo '<i class="icon icon-paper slide-icon">';
                                                   echo '<img src="'.$value['image']['url'].'">';
                                                   echo '</i>';
                                                 }
                                                 else if(!empty ($value['image']) ){
                                                   echo '<i class="icon  slide-logo">';
                                                   echo '<img src="'.$value['image'].'">';
                                                   echo '</i>';
                                                   
                                                   
                                                   
                                                 }else{

                                                 }
                                                 ?>
                                                 
                                                 <span class="quote-content ">
                                                  <?php echo $value['quote'];?>.
                                                </span>
                                                <cite>
                                                  <span class="person"><?php echo $value['name']; ?></span>
                                                  <?php if($value['company']) {
                                                    echo '<span class="organization">"'.$value['postition'].'" &nbsp; "'.$value['company'].'"</span>';
                                                  }else{

                                                    $length = count($value['cat']);
                                                    for ($i = 0; $i < $length; $i++) {
                                                     echo '<span class="organization">"'.$value['cat'][$i].'"&nbsp;</span>';
                                                     
                                                   }
                                                 }
                                                 ?>
                                               </cite>
                                             </blockquote>
                                           </div>
                                           <?php 

                                           $t++; 
                                         endif;
                                       endforeach;
                                     endforeach;
                                     
                                     ?>
                                   </div>
                                   <!--  <div id="cycle-pager-4823" class="cycle-pager cycle-pager-4823 cycle-pager--right"></div> -->
                                 </div>
                                 <a name="skip-c4823"></a>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>

                       <?php 
                       $visit_heading = get_field('visit_heading');
                       $visit_sub_heading = get_field('visit_sub_heading');
                       $visit_map = get_field('map');
                       ?>
                       <div id="c4826" class="layout-container   lc__theme-color-2 text-transparent lc__top-offset-1 support-container  lc__stack-layer-0 lc__bgimage-bottom-right lc__mobile-image-opacity-25  " style="" data-image-animation="bigFadeInAndSlideInRight" data-content-animation="fadeInAndSlideInRight">
                        <div class="lc__background-2  height-default"></div>
                        
                        <div class="lc__background lc__wider  lazyload visit-map" data-sizes="auto">
                          <div class="map_location">
                            <img src="/wp-content/themes/lz-child/img/map1.png" id="div1" style="position: absolute;">
                            <div class="inner_div" id="image1" style="visibility:hidden"><img src="/wp-content/themes/lz-child/img/map2.png"></div>
                            <div class="inner_div" id="image2" style="visibility:hidden"><img src="/wp-content/themes/lz-child/img/map3.png"></div>
                            <div class="inner_div"  id="image3" style="visibility:hidden"> <img src="/wp-content/themes/lz-child/img/map4.png"></div>
                            <div class="inner_div"  id="image4" style="visibility:hidden"> <img src="/wp-content/themes/lz-child/img/map5.png"></div>
                            <div class="inner_div"  id="image5" style="visibility:hidden"> <img src="/wp-content/themes/lz-child/img/map6.png"></div>
                            <div class="inner_div"  id="image6" style="visibility:hidden"> <img src="/wp-content/themes/lz-child/img/map7.png"></div>
                            <div class="inner_div"  id="image7" style="visibility:hidden"> <img src="/wp-content/themes/lz-child/img/map8.png"></div>
                          </div>
                        </div>
                        
                        <div class="lc__inner-wrap lc__pad-top-25 lc__pad-bottom-3   ">
                          <div class="row collapse lc__content  narrow-left">
                            <div class="columns lc__content-offset-top-0 lc__content-bottom-up-0">
                              <div class="lc__content-inner-wrap">
                                <section id="c4825" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-center tai__image-above visit-content">
                                  <div class="ce-content rte-content rte-text-small ">
                                    <div class="headline-group with-rule ">
                                      
                                      <div class="subtitle">OUR LOCATIONS</div>
                                      <h2 class="h1-match title">Plants <span class="break"></span>and Offices</h2>
                                      <span class="rule"></span>
                                    </div>


                                    <p class="indent great">
                                     <?php echo $visit_sub_heading; ?>
                                   </p>
                                   <ul class="arrow-list map-type visit-list ">
                                     <?php
                                     if( have_rows('parts_listing') ):
                                                                // loop through the rows of data
                                      $links = 1;
                                      while ( have_rows('parts_listing') ) : the_row();
                                                                      // display a sub field value
                                        $parts_name = get_sub_field('parts_name');  
                                        $parts_address = get_sub_field('parts_address'); 
                                        echo '<li  id="list'.$links.'"><a href="#" target="_self" class="internal-link map-link"><span>/</span> '.$parts_name.' </a></li>';
                                        $links++;
                                      endwhile;
                                    else :
                                                                  // no rows found
                                    endif;
                                    ?>
                                  </ul>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><!-- end .columns -->
                </main><!-- end #content-block-1 -->
              </div><!-- end .columns -->
            </div><!-- end #content-wrap -->

          <?php } get_footer(); ?>
