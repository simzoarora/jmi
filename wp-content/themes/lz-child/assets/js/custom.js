function googleTranslateElementInit2() {
    new google.translate.TranslateElement({
        pageLanguage: 'en',
        autoDisplay: false
    }, 'google_translate_element2');
}

function GTranslateGetCurrentLang() {
    var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');
    console.log(keyValue);
    return keyValue ? keyValue[2].split('/')[2] : null;
}

function GTranslateFireEvent(element, event) {
    try {
        if (document.createEventObject) {
            var evt = document.createEventObject();
            element.fireEvent('on' + event, evt)
        } else {
            var evt = document.createEvent('HTMLEvents');
            evt.initEvent(event, true, true);
            element.dispatchEvent(evt)
        }
    } catch (e) {}
}

function doGTranslate(lang_pair) {
    if (lang_pair.value) lang_pair = lang_pair.value;
    if (lang_pair == '') return;
    var lang = lang_pair.split('|')[1];
    if (GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0]) return;
    var teCombo;
    var sel = document.getElementsByTagName('select');
    for (var i = 0; i < sel.length; i++)
        if (/goog-te-combo/.test(sel[i].className)) {
            teCombo = sel[i];
            break;
        }
    if (document.getElementById('google_translate_element2') == null || document.getElementById('google_translate_element2').innerHTML.length == 0 || teCombo.length == 0 || teCombo.innerHTML.length == 0) {
        setTimeout(function () {
            doGTranslate(lang_pair)
        }, 500)
    } else {
        teCombo.value = lang;
        GTranslateFireEvent(teCombo, 'change');
        GTranslateFireEvent(teCombo, 'change')
    }
}


(function ($) {

    $(document).ready(function(){
//        navbar-menu a link self removed
//        $('.nav-global__list li a ').removeAttr('href');
// right side buttons removed from quote-box
$('.quote-box a').remove();
        
        
            // on hover change update colrors link
             $(".slide-points")
                    .mouseenter(function () {
                        $(this).find("span").css("color", "#ffb000");
                    })
                    .mouseleave(function () {
                        $(this).find("span").css("color", "#fff");
                    });

            $(".explore-points")
                    .mouseenter(function () {
                        $(this).find("span").css("color", "#0a1f54");
                    })
                    .mouseleave(function () {
                        $(this).find("span").css("color", "#2aa9e0");
                    });

            $(".future-link")
                    .mouseenter(function () {
                        $(this).find("span").css("color", "#0a1f54");
                    
                    })
                    .mouseleave(function () {
                        $(this).find("span").css("color", "#2aa9e0");
                     
                    });

            $(".map-link")
                    .mouseenter(function () {
                        $(this).find("span").css("color", "#ffb000");
                    })
                    .mouseleave(function () {
                        $(this).find("span").css("color", "#2aa9e0");
                    });

            //  on hover update history background image
            $(".history-image, .lc__sidebar").on({
                mouseenter: function () {
                   $('.new').hide();
                   $('.old-text').css("color", "#fff");
                   $('.new-text').css("color", "#8a8c8e");
                  $('.old').show();
                },
                mouseleave: function () {
                    $('.old').hide();
                    $('.new-text').css("color", "#fff");
                    $('.old-text').css("color", "rgb(82, 82, 82)");
                    $('.new').show();
                }
            });
            
            $('.history-image, .lc__sidebar').on('click touchstart', function() {
                   $('.new').hide();
                   $('.old-text').css("color", "#fff");
                   $('.new-text').css("color", "#bcbbbb");
                  $('.old').show();
                });

//                top slider
            $(document).on("swiperight", ".top-slider", function (event) {
                $('.cycle-next').trigger('click'); 
                console.log('hihiihih');
                    });
            $(document).on("swipeleft", ".top-slider", function (event) {
                $('.cycle-prev').trigger('click');
                    });
        //second slider
        $(document).on("swiperight", ".second-slider", function (event) {
                $('.right-second-arrow').trigger('click');
                    });
            $(document).on("swipeleft", ".second-slider", function (event) {
                $('.left-second-arrow').trigger('click');
                    });
       
    
        
            // map listing hovr event's
           $("#list1").on({
                mouseenter: function() {
                $("#image5").css({visibility: 'visible'}); 
                // $("#image1").css({transform: 'none'});
                },mouseleave: function() {
                 $("#image5").css({visibility: 'hidden'}); 
                    // $("#image1").css({transform: 'translate(-50px, -80px)'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'hidden'}); 
                $("#image2").css({visibility: 'hidden'}); 
                $("#image3").css({visibility: 'hidden'}); 
                $("#image4").css({visibility: 'hidden'}); 
                $("#image5").css({visibility: 'visible'}); 
                $("#image6").css({visibility: 'hidden'}); 
                $("#image7").css({visibility: 'hidden'}); 
                // $("#image1").css({transform: 'none'});
                }
            })
            $("#list2").on({
                mouseenter: function() {
                $("#image3").css({visibility: 'visible'}); 
                // $("#image2").css({transform: 'none'});
                },mouseleave: function() {
                    // $("#image2").css({transform: 'translate(-50px, -80px)'});
                    $("#image3").css({visibility: 'hidden'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'hidden'}); 
                $("#image2").css({visibility: 'hidden'}); 
                $("#image3").css({visibility: 'visible'}); 
                $("#image4").css({visibility: 'hidden'}); 
                $("#image5").css({visibility: 'hidden'}); 
                $("#image6").css({visibility: 'hidden'}); 
                $("#image7").css({visibility: 'hidden'}); 
                // $("#image1").css({transform: 'none'});
                }
                
            })
            
             $("#list3").on({
                mouseenter: function() {
                $("#image4").css({visibility: 'visible'}); 
                // $("#image3").css({transform: 'none'});
                },mouseleave: function() {
                    // $("#image3").css({transform: 'translate(-50px, -80px)'});
                    $("#image4").css({visibility: 'hidden'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'hidden'}); 
                $("#image2").css({visibility: 'hidden'}); 
                $("#image3").css({visibility: 'hidden'}); 
                $("#image4").css({visibility: 'visible'}); 
                $("#image5").css({visibility: 'hidden'}); 
                $("#image6").css({visibility: 'hidden'}); 
                $("#image7").css({visibility: 'hidden'}); 
                // $("#image3").css({transform: 'none'});
                }
            })
            
             $("#list4").on({
                mouseenter: function() {
                $("#image2").css({visibility: 'visible'}); 
                // $("#image4").css({transform: 'none'});
                },mouseleave: function() {
                    // $("#image4").css({transform: 'translate(-50px, -80px)'});
                    $("#image2").css({visibility: 'hidden'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'hidden'}); 
                $("#image2").css({visibility: 'visible'}); 
                $("#image3").css({visibility: 'hidden'}); 
                $("#image4").css({visibility: 'hidden'}); 
                $("#image5").css({visibility: 'hidden'}); 
                $("#image6").css({visibility: 'hidden'}); 
                $("#image7").css({visibility: 'hidden'}); 
                // $("#image4").css({transform: 'none'});
                }
            })
            
             $("#list5").on({
                mouseenter: function() {
                $("#image7").css({visibility: 'visible'}); 
                // $("#image5").css({transform: 'none'});
                },mouseleave: function() {
                    // $("#image5").css({transform: 'translate(-50px, -80px)'});
                    $("#image7").css({visibility: 'hidden'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'hidden'}); 
                $("#image2").css({visibility: 'hidden'}); 
                $("#image3").css({visibility: 'hidden'}); 
                $("#image4").css({visibility: 'hidden'}); 
                $("#image5").css({visibility: 'hidden'}); 
                $("#image6").css({visibility: 'hidden'}); 
                $("#image7").css({visibility: 'visible'}); 
                // $("#image5").css({transform: 'none'});
                }
            })
            
             $("#list6").on({
                mouseenter: function() {
                $("#image6").css({visibility: 'visible'}); 
                // $("#image6").css({transform: 'none'});
                },mouseleave: function() {
                    // $("#image6").css({transform: 'translate(-50px, -80px)'});
                    $("#image6").css({visibility: 'hidden'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'hidden'}); 
                $("#image2").css({visibility: 'hidden'}); 
                $("#image3").css({visibility: 'hidden'}); 
                $("#image4").css({visibility: 'hidden'}); 
                $("#image5").css({visibility: 'hidden'}); 
                $("#image6").css({visibility: 'visible'}); 
                $("#image7").css({visibility: 'hidden'});  
                // $("#image6").css({transform: 'none'});
                }
            })
            $("#list7").on({
                mouseenter: function() {
                $("#image1").css({visibility: 'visible'}); 
                // $("#image6").css({transform: 'none'});
                },mouseleave: function() {
                    // $("#image6").css({transform: 'translate(-50px, -80px)'});
                    $("#image1").css({visibility: 'hidden'}); 
                },
                click: function() {
                $("#image1").css({visibility: 'visible'}); 
                $("#image2").css({visibility: 'hidden'}); 
                $("#image3").css({visibility: 'hidden'}); 
                $("#image4").css({visibility: 'hidden'}); 
                $("#image5").css({visibility: 'hidden'}); 
                $("#image6").css({visibility: 'hidden'}); 
                $("#image7").css({visibility: 'hidden'});
                // $("#image6").css({transform: 'none'});
                }
            })
            
            $(".nav-global__link").removeAttr('title');



    });
    

}(jQuery));



