<?php
/**
 * The template for displaying all category pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package lz-child
 */
get_header();

// get the current taxonomy term
$term = get_queried_object();

$bannerimg = get_field('banner_image', $term);
$bannertext = get_field('banner_text', $term);
$quotetext = get_field('get_a_quote_text', $term);
$quoteurl = get_field('get_a_quote_url', $term);
?>
<div class="inner-wrap medium-24 news-update single-news-category">
    <div class="banner-section">
        <img src="<?php echo $bannerimg['url']; ?>">
        <div class="banner-text-section">
            <div class="banner-text-container row">
                <h6><?php the_title(); ?></h6>
                <h1><?php echo $bannertext; ?></h1>
            </div></div>

    </div><!-- banner-section end -->
    <div class="row news-section">
        <div class="feature-footer" style="position: relative !important;">
            <div class="inner-wrap">
                <a class="plan-button button button-table large" href="/contact-us" style="height: 70px;">
                    <span class="first-icon-cell get-first">
                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                    </span>
                    <span class="text-cell text">Get a Quote</span>
                    <span class="last-icon-cell get-last">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div><!-- end .inner-wrap -->
        </div>
        <div class="columns news-columns">
            <div class="lc__intro-content">
                <?php
                $blogtime = date('Y');
                $prev_limit_year = $blogtime - 1;
                $prev_month = '';
                $prev_year = '';
                ?>
                <?php if (have_posts()) : ?>
                    <?php /* Start the Loop */ ?>
                    <?php
                    while (have_posts()) : the_post();
                        echo "<div class='month-news-container' >";
                        if (get_the_time('F') != $prev_month || get_the_time('Y') != $prev_year && get_the_time('Y') == $prev_limit_year) {
                            echo "<h2 class='month-title'>" . get_the_time('F') . "</h2>\n\n";
                        }

                        $meta = get_post_meta(get_the_ID());
                        $file = get_field('upload_pdf_file');

                        ?>


                        <div class="monthe-single-news">
                            <div class="month-left-details">
                                <div class="news-date"><span><?php echo get_the_date('M. j, Y'); ?></span></div>
                                <div class="news-category">
                                    <a href="<?php echo $cat_url; ?>" class="<?php echo $term->category_nicename; ?>"><?php single_cat_title(); ?></a>
                                </div>
                            </div>
                            <?php if($meta['select_option_on_click_action'][0] == 'full_news_read'): ?>
                                <div class="news-details" ><a style="display:flex;" href="<?php the_permalink(); ?>"><span style="margin-right:10px;">/</span><?php the_excerpt(); ?></a></div>
                            <?php elseif( $meta['select_option_on_click_action'][0] == 'external_url_link' ):?>
                                <div class="news-details" ><a style="display:flex;" href="<?php echo $meta['external_url_link'][0]; ?>" target="_blank"><span style="margin-right:10px;">/</span><?php the_excerpt(); ?></a></div>
                            <?php else: ?>
                                <div class="news-details" ><a style="display:flex;" href="<?php echo $file['url']; ?>" target="_blank"><span style="margin-right:10px;">/</span><?php the_excerpt(); ?></a></div>
                            <?php endif;?>
                        </div>

                        <?php echo "</div>"; ?>
                        <?php //get_template_part( 'content', get_post_format() ); ?>
                    <?php endwhile; ?>
                    <?php the_posts_pagination(); ?>
                <?php else : ?>
                    <?php get_template_part('no-results', 'archive'); ?>
                <?php endif; ?>
            </div>

            <div class="lc__sidebar text-light">

                <?php if (is_active_sidebar('news-update-sidebar')) : ?>
                    <?php dynamic_sidebar('news-update-sidebar'); ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>





<?php get_footer(); ?>