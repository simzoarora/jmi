<?php
/**
 * Template Name: Contact Page
 * The template for displaying Contact Page.
 *
 *
 * @package lz Team
 */


get_header(); 

$bannerimg = get_field('banner_image');
$bannertext = get_field('banner_text');
$quotetext = get_field('get_a_quote_text');
$quoteurl = get_field('get_a_quote_url');
?>
<div class="inner-wrap medium-24 news-update">
	<div class="banner-section">
		<img src="<?php echo $bannerimg['url']; ?>">
	</div><!-- banner-section end -->
	<div class="row news-section">
		<div class="columns news-columns">
			<?php echo the_content(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>