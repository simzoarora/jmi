<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package lz Team
 */
?>
<footer id="footer">
    <div class="newsletter-social-wrap row">
        <div class="medium-24 columns">
            <?php dynamic_sidebar('footer-contact'); ?>

        </div><!-- end .columns -->
    </div><!-- end .newsletter-social-wrap -->
    <div class="row footer-link">
        <div class="medium-5 columns">
            <h5 class="column-header">About Us</h5>
            <div class="footer-link-container">
                <ul class="footer-list">
                    <?php
                    $global_nav = get_pages();
                    foreach ($global_nav as $key => $nav) {
                        if ($nav->post_name == 'about-jmi') {
                            echo '<li><a href="/about-jmi">' . $nav->post_title . '</a></li>';
                            $args = array(
                                'post_type' => 'page',
                                'posts_per_page' => -1,
                                'post_parent' => $nav->ID,
                                'order' => 'ASC',
                                'orderby' => 'menu_order'
                            );
                            $parent = new WP_Query($args);
                            if ($parent->have_posts()) :
                                ?>
                                <?php while ($parent->have_posts()) : $parent->the_post(); ?>
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php endwhile; ?>

                                <?php
                            endif;
                            wp_reset_query();
                        }
                    }
                    ?>
                </ul>
            </div>
        </div><!-- end .columns -->
        <div class="medium-5 columns">
            <h5 class="column-header">Our Products</h5>
            <div class="footer-link-container">
                <ul class="footer-list">
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'orderby' => 'title', 'order' => 'ASC'
                    );
                    $loop = new WP_Query($args);
                    ?>
                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>

                        <li><a href="<?php echo get_post_permalink(); ?>"><?php echo the_title(); ?></a></li>

                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </ul>
            </div>
        </div><!-- end .columns  -->
        <div class="medium-9 columns">
            <h5 class="column-header">Our Professions</h5>
            <div class="medium-12 columns footer-link-container">
                <ul class="footer-list">
                    <?php
                    $args = array(
                        'post_type' => 'professions',
                        'post_status' => 'publish',
                        'posts_per_page' => 6,
                        'orderby' => 'title', 'order' => 'ASC'
                    );
                    $loop = new WP_Query($args);
                    ?>
                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>

                        <li><a href="<?php echo get_post_permalink(); ?>"><?php echo the_title(); ?></a></li>

                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </ul>
            </div>
            <div class="medium-12 columns footer-link-container">
                <ul class="footer-list">
                    <?php
                    $args = array(
                        'post_type' => 'professions',
                        'post_status' => 'publish',
                        'offset' => 6,
                        'posts_per_page' => 7,
                        'orderby' => 'title', 'order' => 'ASC'
                    );
                    $loop = new WP_Query($args);
                    ?>
                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                        <?php if (get_the_title() == 'Precision Injection Molding'): ?>

                        <?php else: ?>
                            <li><a href="<?php echo get_post_permalink(); ?>"><?php echo the_title(); ?></a></li>
                        <?php endif; ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>

                </ul>
            </div>
        </div><!-- end .columns  -->
        <div class="medium-5 columns">
            <h5 class="column-header">News and Updates</h5>
            <div class="footer-link-container">

                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer_news_bottom',
                    'menu_class' => 'footer-list',
                    'container' => '',
                    'container_class' => '',
                    'container_id' => '',
                ));
                ?>
            </div>
        </div><!-- end .columns -->
    </div><!-- end row -->
    <div data-footer-end class="footer-end row">
        <div class="large-10 large-push-8 medium-24 columns">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'footer_bottom',
                'menu_class' => 'nav-utility inline-list privacy',
                'container' => '',
                'container_class' => '',
                'container_id' => '',
                'after' => '<i class="fa fa-angle-right right-icon" aria-hidden="true"></i>'
            ));
            ?>

        </div><!-- end .columns -->

        <div class="large-6 medium-24 columns translate-btn-column">
            <div data-translate-btn class="translate-btn" style="display: block;">
                <span class="last-icon-cell"><i class="fa fa-angle-down" aria-hidden="true" style="color: #fff;font-size: 22px;"></i></span>
                <!-- <div id="google_translate_element"></div> -->
                <select onchange="doGTranslate(this);" class="notranslate goog-te-combo lang" id="gtranslate_selector" >
                    <option value="">Select Language</option>
                    <option value="en|en">English</option>
                    <option value="en|zh-CN">Chinese (Simplified)</option>
                    <option value="en|hmn">Hmong</option>
                </select>
                <div id="google_translate_element2"></div>
            </div>
        </div><!-- end .columns -->
    </div><!-- end .row -->
</footer>
</div><!-- end .off-canvas-wrap -->
</div><!-- end .inner-wrap -->

<?php wp_footer(); ?>
<script src="//code.jquery.com/jquery-2.2.4.min.js"></script>

<!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.5/jquery.mobile.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sticky-js/1.2.0/sticky.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lz-child/assets/js/all.min.1537293121.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-touch-events/1.0.5/jquery.mobile-events.js"></script>
 <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.js"></script>--> 
 <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.scrollVert.js"></script>--> 
 

<script type="text/javascript">

                    piAId = '407342';
                    piCId = '78509';
                    piHostname = 'pi.pardot.com';

                    (function () {
                        function async_load() {
                            var s = document.createElement('script');
                            s.type = 'text/javascript';
                            s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
                            var c = document.getElementsByTagName('script')[0];
                            c.parentNode.insertBefore(s, c);
                        }
                        if (window.attachEvent) {
                            window.attachEvent('onload', async_load);
                        } else {
                            window.addEventListener('load', async_load, false);
                        }
                    })();

</script>
<script type="text/javascript">
    jQuery(document).on('personas:ready', function () {
        var $ = jQuery;

        $('#slideshow-4811').cycle();
    });
    jQuery(document).ready(function ($) {
        var progressBarPath;

        if ($('html').hasClass('personas-ready')) {

            $('#slideshow-4811').cycle();

        }

        function getProgressBarPath() {
            if (!progressBarPath) {
                var arrowAnimateTime = 8500;
                progressBarPath = new ProgressBar.Path(scene4811c.querySelector('#progress-outer-circle-4811'), {
                    duration: arrowAnimateTime,
                });
            }

            return progressBarPath;
        }

        var scene4811c = $('#pause-icon-4811').get(0),
                slideshow4811c = $('#slideshow-4811');

        var waypoints4811c = $('#slideshow-4811').waypoint(function (direction) {
            if (scene4811c && scene4811c.querySelector('#progress-outer-circle-4811')) {
                var path = getProgressBarPath();
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
            $('#slideshow-4811').cycle('resume');
        }, {
            offset: 'bottom-in-view'
        })
        slideshow4811c.on('cycle-before', function (e, opts, outgoingSlideEl, incomingSlideEl, forwardFlag) {
            if (!(slideshow4811c.is('.cycle-paused')) && $('#pause-icon-4811').is(':visible')) {
                var arrowAnimateTime = 8500;
                if (scene4811c && scene4811c.querySelector('#progress-outer-circle-4811')) {
                    var path = getProgressBarPath(true);
                    path.set(0);
                    path.animate(1.0, function () {
                        path.set(0);
                    });
                }
            }

            $video = $(incomingSlideEl).find('video');
            if ($video.length) {
                $video.each(function () {
                    $(this).show();
                    this.play();
                });
            }

        });
        slideshow4811c.on('cycle-after', function (e, opts, outgoingSlideEl, incomingSlideEl, forwardFlag) {
            $video = $(outgoingSlideEl).find('video');
            if ($video.length) {
                $video.each(function () {
                    $(this).hide();
                    this.pause();
                });
            }
        });
        slideshow4811c.on('cycle-initialized', function (e, opts) {
            $video = $(this).find('.slide:first video');
            if ($video.length) {
                $video.each(function () {
                    this.play();
                });
            }
        });

        slideshow4811c.on('cycle-paused', function (e, opts) {
            $('#pause-4811').hide();
            $('#resume-4811').show();
        });

        slideshow4811c.on('cycle-resumed', function (e, opts) {
            $('#resume-4811').hide();
            $('#pause-4811').show();
        });

        slideshow4811c.on('cycle-next cycle-prev cycle-pager-activated', function (e, opts) {
            slideshow4811c.cycle('pause');

            // Reset the animation when paused.
            if (scene4811c && scene4811c.querySelector('#progress-outer-circle-4811')) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
            }
        });
        $('body').on('addtovisit:opening', function () {
            slideshow4811c.cycle('pause');

            // Reset the animation when paused.
            if (scene4811c && scene4811c.querySelector('#progress-outer-circle-4811')) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
            }
        });
        $('body').on('addtovisit:closed', function () {
            slideshow4811c.cycle('resume');
            var arrowAnimateTime = 8500;
            if (scene4811c) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
        });
        slideshow4811c.css('height', slideshow4811c.find('.slide.first').height()).cycle();
        setTimeout(function () {
            slideshow4811c.css('height', 'auto');
        }, 500);
    });
</script>
<script type="text/javascript">
    jQuery(document).on('personas:ready', function () {
        var $ = jQuery;

        $('#slideshow-17856').cycle();
        $('#slideshow-17856-small').cycle();
        $('#slideshow-17856-text').cycle();
    });

    jQuery(document).ready(function ($) {
        var progressBarPath;

        if ($('html').hasClass('personas-ready')) {

            $('#slideshow-17856').cycle();
            $('#slideshow-17856-small').cycle();
            $('#slideshow-17856-text').cycle();

        }

        function getProgressBarPath() {
            if (!progressBarPath) {
                var arrowAnimateTime = 8500;
                progressBarPath = new ProgressBar.Path(scene17856c.querySelector('#progress-outer-circle-17856'), {
                    duration: arrowAnimateTime,
                });
            }

            return progressBarPath;
        }

        var scene17856c = $('#pause-icon-17856').get(0),
                slideshow17856c = $('#slideshow-17856'),
                slideshow17856cs = $('#slideshow-17856-small'),
                slideshow17856tx = $('#slideshow-17856-text');
        ;
        var waypoints17856c = $('#slideshow-17856').waypoint(function (direction) {
            var arrowAnimateTime = 8500;
            if (scene17856c && scene17856c.querySelector('#progress-outer-circle-17856')) {
                var path = getProgressBarPath();
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
            $('#slideshow-17856').cycle('resume');
            $('#slideshow-17856-small').cycle('resume');
            $('#slideshow-17856-text').cycle('resume');
        }, {
            offset: 'bottom-in-view'
        })
        slideshow17856c.on('cycle-before', function (e, opts) {
            if (!slideshow17856c.is('.cycle-paused')) {
                var path = getProgressBarPath();
                path.set(0);
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
        });

        slideshow17856c.on('cycle-next cycle-prev cycle-pager-activated', function (e, opts) {
            slideshow17856c.cycle('pause');
            slideshow17856cs.cycle('pause');
            slideshow17856tx.cycle('pause');

            // Reset the animation when paused.
            if (scene17856c && scene17856c.querySelector('#progress-outer-circle-17856')) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
            }
        });

        slideshow17856c.on('cycle-pager-activated', function (e, opts) {
            var targetIndex = opts.currSlide % opts.slideCount
            slideshow17856cs.cycle('goto', targetIndex);
        });

        slideshow17856c.on('cycle-paused', function (e, opts) {
            slideshow17856cs.cycle('pause');
            slideshow17856tx.cycle('pause');
            $('#pause-17856').hide();
            $('#resume-17856').show();
        });

        slideshow17856c.on('cycle-resumed', function (e, opts) {
            slideshow17856cs.cycle('resume');
            slideshow17856tx.cycle('resume');
            $('#resume-17856').hide();
            $('#pause-17856').show();
        });

        $('body').on('addtovisit:opening', function () {
            slideshow17856c.cycle('pause');
            slideshow17856cs.cycle('pause');
            slideshow17856tx.cycle('pause');

            // Reset the animation when paused.
            if (scene17856c && scene17856c.querySelector('#progress-outer-circle-17856')) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
            }
        });
        $('body').on('addtovisit:closed', function () {
            var arrowAnimateTime = 8500;
            if (scene17856c) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
            $('#slideshow-17856').cycle('resume');
            $('#slideshow-17856-small').cycle('resume');
            $('#slideshow-17856-text').cycle('resume');
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).on('personas:ready', function () {
        var $ = jQuery;

        $('#slideshow-4823').cycle();
    });
    jQuery(document).ready(function ($) {
        var progressBarPath;

        if ($('html').hasClass('personas-ready')) {

            $('#slideshow-4823').cycle();

        }

        function getProgressBarPath() {
            if (!progressBarPath) {
                var arrowAnimateTime = 5500;
                progressBarPath = new ProgressBar.Path(scene4823c.querySelector('#progress-outer-circle-4823'), {
                    duration: arrowAnimateTime,
                });
            }

            return progressBarPath;
        }

        var scene4823c = $('#pause-icon-4823').get(0),
                slideshow4823c = $('#slideshow-4823');

        var waypoints4823c = $('#slideshow-4823').waypoint(function (direction) {
            if (scene4823c && scene4823c.querySelector('#progress-outer-circle-4823')) {
                var path = getProgressBarPath();
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
            $('#slideshow-4823').cycle('resume');
        }, {
            offset: 'bottom-in-view'
        })
        slideshow4823c.on('cycle-before', function (e, opts, outgoingSlideEl, incomingSlideEl, forwardFlag) {
            if (!(slideshow4823c.is('.cycle-paused')) && $('#pause-icon-4823').is(':visible')) {
                var arrowAnimateTime = 5500;
                if (scene4823c && scene4823c.querySelector('#progress-outer-circle-4823')) {
                    var path = getProgressBarPath(true);
                    path.set(0);
                    path.animate(1.0, function () {
                        path.set(0);
                    });
                }
            }

            $video = $(incomingSlideEl).find('video');
            if ($video.length) {
                $video.each(function () {
                    $(this).show();
                    this.play();
                });
            }

        });
        slideshow4823c.on('cycle-after', function (e, opts, outgoingSlideEl, incomingSlideEl, forwardFlag) {
            $video = $(outgoingSlideEl).find('video');
            if ($video.length) {
                $video.each(function () {
                    $(this).hide();
                    this.pause();
                });
            }
        });
        slideshow4823c.on('cycle-initialized', function (e, opts) {
            $video = $(this).find('.slide:first video');
            if ($video.length) {
                $video.each(function () {
                    this.play();
                });
            }
        });

        slideshow4823c.on('cycle-paused', function (e, opts) {
            $('#pause-4823').hide();
            $('#resume-4823').show();
        });

        slideshow4823c.on('cycle-resumed', function (e, opts) {
            $('#resume-4823').hide();
            $('#pause-4823').show();
        });

        slideshow4823c.on('cycle-next cycle-prev cycle-pager-activated', function (e, opts) {
            slideshow4823c.cycle('pause');

            // Reset the animation when paused.
            if (scene4823c && scene4823c.querySelector('#progress-outer-circle-4823')) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
            }
        });
        $('body').on('addtovisit:opening', function () {
            slideshow4823c.cycle('pause');

            // Reset the animation when paused.
            if (scene4823c && scene4823c.querySelector('#progress-outer-circle-4823')) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
            }
        });
        $('body').on('addtovisit:closed', function () {
            slideshow4823c.cycle('resume');
            var arrowAnimateTime = 5500;
            if (scene4823c) {
                var path = getProgressBarPath();
                path.stop();
                path.set(0);
                path.animate(1.0, function () {
                    path.set(0);
                });
            }
        });
        slideshow4823c.css('height', slideshow4823c.find('.slide.first').height()).cycle();
        setTimeout(function () {
            slideshow4823c.css('height', 'auto');
        }, 500);
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.4/jquery.sticky.js"></script>

<script type="text/javascript">
    (function ($) {

        /*
         *  new_map
         *
         *  This function will render a Google Map onto the selected jQuery element
         *
         *  @type	function
         *  @date	8/11/2013
         *  @since	4.3.0
         *
         *  @param	$el (jQuery element)
         *  @return	n/a
         */

        function new_map($el) {

            // var
            var $markers = $el.find('.marker');


            // vars
            var args = {
                zoom: 2,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };


            // create map	        	
            var map = new google.maps.Map($el[0], args);


            // add a markers reference
            map.markers = [];


            // add markers
            $markers.each(function () {

                add_marker($(this), map);

            });


            // center map
            center_map(map);


            // return
            return map;

        }

        /*
         *  add_marker
         *
         *  This function will add a marker to the selected Google Map
         *
         *  @type	function
         *  @date	8/11/2013
         *  @since	4.3.0
         *
         *  @param	$marker (jQuery element)
         *  @param	map (Google Map object)
         *  @return	n/a
         */

        function add_marker($marker, map) {

            // var
            var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

            // create marker
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });

            // add to array
            map.markers.push(marker);

            // if marker contains HTML, add it to an infoWindow
            if ($marker.html())
            {
                // create info window
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });

                // show info window when marker is clicked
                google.maps.event.addListener(marker, 'click', function () {

                    infowindow.open(map, marker);

                });
            }

        }

        /*
         *  center_map
         *
         *  This function will center the map, showing all markers attached to this map
         *
         *  @type	function
         *  @date	8/11/2013
         *  @since	4.3.0
         *
         *  @param	map (Google Map object)
         *  @return	n/a
         */

        function center_map(map) {

            // vars
            var bounds = new google.maps.LatLngBounds();

            // loop through all markers and create bounds
            $.each(map.markers, function (i, marker) {

                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

                bounds.extend(latlng);

            });

            // only 1 marker?
            if (map.markers.length == 1)
            {
                // set center of map
                map.setCenter(bounds.getCenter());
                map.setZoom(16);
            } else
            {
                // fit to bounds
                map.fitBounds(bounds);
            }

        }

        /*
         *  document ready
         *
         *  This function will render each map when the document is ready (page has loaded)
         *
         *  @type	function
         *  @date	8/11/2013
         *  @since	5.0.0
         *
         *  @param	n/a
         *  @return	n/a
         */
// global var
        var map = null;

        $(document).ready(function () {

            $('.acf-map').each(function () {

                map = new_map($(this));

            });


            function fixPlanButton() {
                // Home page
                let elPosition = $('.lc__background-2').position();
                if (elPosition) {
                    $('.feature-footer.home-quote-btn').css('left', elPosition.left);
                }

                // News update page
                let sidebarPosition = $('.news-section .lc__sidebar').position()
                let sidebarWidth = $('.news-section .lc__sidebar').width();
                let windowWidth = $(window).width();
                if (sidebarPosition && windowWidth > '768') {
                    $('.news-update .feature-footer').css('left', sidebarPosition.left);
                    $('.news-update .feature-footer .plan-button').css('width', sidebarWidth);
                } else if (sidebarPosition && windowWidth == '768') {
                    $('.news-update .feature-footer').css('left', sidebarPosition.left);
                    $('.news-update .feature-footer .plan-button').css('width', sidebarWidth + 30);
                } else {
                    $('.news-update .feature-footer').css('left', 0);
                    $('.news-update .feature-footer .plan-button').css('width', '100%');
                }
            }
            fixPlanButton();

            $(window).resize(function () {
                fixPlanButton();
            });

            $(window).scroll(function () {
                if ($(window).width() > '767') {
                    var limit_bottom = $('#footer').height();
                    $('#my_categories-2').sticky({topSpacing: 50, bottomSpacing: limit_bottom + 130});
                }
            })

            $(".contact-us-page-submit").css('background-color', '#e8e6e6 !important');

        });
  
    })(jQuery);
    
</script>


</body>
</html>