<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//heapler function
if(!function_exists('pr')){
	function pr($txt){
		echo "<pre>";
		print_r($txt);

		return;
	}
}

// include 'inc/helper.php';
include 'inc/custom_nav.php';
include 'inc/shortcode.php';

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );

    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css',99 );


function child_thm_enqueue(){
 
  wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );

  wp_enqueue_style('all-min', get_stylesheet_directory_uri(). '/assets/css/all.min.1537293121.css');
//  wp_enqueue_style('custom-theme', get_stylesheet_directory_uri(). '/assets/css/style.css');
  wp_enqueue_style('new-custom-theme', get_stylesheet_directory_uri(). '/assets/css/new-style.css');

  // wp_enqueue_script('mobile', '//cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.5/jquery.mobile.js');

  wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyC06gnK3AC51ipkMFbItNOJyw0WyeYTz9E');
    wp_enqueue_script('googleTranslate', '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2');
    
  wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js', true); 

  wp_register_script('all-min-js', get_stylesheet_directory_uri() .'/assets/js/all.min.1537293121.js', true); 
  // wp_enqueue_script('all-min-js');
  wp_register_script('custom-lz', get_stylesheet_directory_uri() .'/assets/js/custom.js', true); 
  wp_enqueue_script('custom-lz');
}

add_action('wp_enqueue_scripts', 'child_thm_enqueue', 99);
// END ENQUEUE PARENT ACTION




// remove parent theme menu's
add_action( 'admin_menu', 'lz_adjust_the_wp_menu', 999 );
function lz_adjust_the_wp_menu() {
	$page = remove_submenu_page( 'themes.php', 'skt_blendit_guide' );
}

// remove parent theme custom stting's
add_action( "customize_register", "lz_theme_customize_register", 20 );
function lz_theme_customize_register( $wp_customize ) {

  $wp_customize->remove_section( 'slider_section' );
  $wp_customize->remove_section( 'section_second' );
}


// Regsiter footer bottm menu
add_action( 'after_setup_theme', 'theme_primary_menu', 20 );
function theme_primary_menu() {
	register_nav_menus( array(
		 'primary' => __( 'Primary Menu', 'lz' ),
		 'footer' => __( 'Footer Menu', 'lz' ),
		 'footer_bottom' => __( 'Footer Bottom Menu', 'lz' ),

	) );
}



// add ancher tag's class
add_filter('wp_nav_menu','add_menuclass');
function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-global__link"', $ulclass);
}

// Regsiter widge area for header
function lz_widgets_init() { 	
	
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'lz' ),
		'description'   => __( 'Appears on blog page sidebar', 'lz' ),
		'id'            => 'sidebar-1',
		'before_widget' => '',		
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Header Widget', 'lz' ),
		'description'   => __( 'Appears on top of the header', 'lz' ),
		'id'            => 'header-widget',
		'before_widget' => '',		
		'before_title'  => '<h3 class="header-title" style="display:none;">',
		'after_title'   => '</h3><aside id="%1$s" class="headwidget %2$s">',
		'after_widget'  => '</aside>',
	) );

	register_sidebar(array(
     'name' => 'Header language',
     'id' => 'header-language',
      'description' => "",
       'before_widget' => '<div id="%1$s" class="widget text-right">',
       'after_widget' => '</div>',
       'before_title' => '<h3 class="group"><span>',
       'after_title' => '</span></h3>'
   ));

	register_sidebar(array(
     'name' => 'Footer Contact',
     'id' => 'footer-contact',
      'description' => "",
       'before_widget' => '<div id="%1$s" class="widget %2$s">',
       'after_widget' => '</div>',
       'before_title' => '<h3 class="group"><span>',
       'after_title' => '</span></h3>'
   ));

    
  register_sidebar( array(
    'name' => __( 'News & Updates Sidebar', 'wpb' ),
    'id' => 'news-update-sidebar',
    'description' => __( 'The main sidebar use for lisitng the news and update category', 'wpb' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
  
) );

	
}
add_action( 'widgets_init', 'lz_widgets_init' );

// change default search form
function lz_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text" for="s">' . __( 'Search for:' ) . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <button id="searchsubmit"><i class="fa fa-search"></i></button>

    </div>
    </form>';

    return $form;
}

add_filter( 'get_search_form', 'lz_search_form', 100 );


// display nav menu descriptions
function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<p class="menu-item-description">' . $item->description . '</p>' . $args->link_after . '</a>', $item_output );
    }
 
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );



//add filter for acf map field call
function my_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyC06gnK3AC51ipkMFbItNOJyw0WyeYTz9E';
  return $api;
} 
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// Adding Excerpts to Pages in WordPress
add_post_type_support( 'page', 'excerpt' );

//remove p tag from the_exceprt content 
remove_filter ('the_exceprt', 'wpautop');
remove_filter('the_content', 'wpautop');



/*
Plugin Name: My Categories Widget
Version: 0.1
*/

class My_Widget_Categories extends WP_Widget {

  function My_Widget_Categories() {
      $widget_ops = array( 'classname' => 'widget_categories', 'description' => __( "News & Updates Category" ) );
      $this->WP_Widget('my_categories', __('News & Updates Category'), $widget_ops);
  }

  function widget( $args, $instance ) {
      extract( $args );

      $title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Categories' ) : $instance['title']);
      $c = $instance['count'] ? '1' : '0';
      $h = $instance['hierarchical'] ? '1' : '0';
      $d = $instance['dropdown'] ? '1' : '0';

      echo $before_widget;
      if ( $title )
          echo $before_title . $title . $after_title;

      $cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h);

?>
      <div class="news-sidebar-container">
<?php
      $cat_args['title_li'] = '';
   
      $categories = get_categories( array(
        'orderby' => 'name',
        'order'   => 'ASC'
    ) );
    echo '<a class="cat-item all-new-update news-link" href="/news-updates"><img src="/wp-content/themes/lz-child/img/all.png"><span>News & Updates</span></a>';
foreach($categories as $cat ){
  if (function_exists('get_wp_term_image'))
  {   
  
      $meta_image = get_wp_term_image($cat->term_id); 
      $cat_url = get_category_link($cat->term_id);
      $cat_name = $cat->name;
      echo '<a href="'.$cat_url.'" class="cat-item news-link '.$cat->category_nicename.'">
      <img src="'.$meta_image.'"><span>'.$cat_name.'</span></a>';
      //It will give category/term image url 
  }
}





      // wp_list_categories(apply_filters('widget_categories_args', $cat_args));
?>
      </div>
<?php
    

      echo $after_widget;
  }

  function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['count'] = $new_instance['count'] ? 1 : 0;
      $instance['hierarchical'] = $new_instance['hierarchical'] ? 1 : 0;
      $instance['dropdown'] = $new_instance['dropdown'] ? 1 : 0;

      return $instance;
  }

  function form( $instance ) {
      //Defaults
      $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
      $title = esc_attr( $instance['title'] );
      $count = isset($instance['count']) ? (bool) $instance['count'] :false;
      $hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
      $dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

      <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"checked />
      <label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>
<?php
  }

}

add_action('widgets_init', create_function('', "register_widget('My_Widget_Categories');"));





