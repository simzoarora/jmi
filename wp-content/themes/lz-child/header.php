<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package lz Team
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"> 
  <?php wp_head(); ?>
</head>
<body id="f1a" <?php body_class('home main bleed bh-color__theme-c20045'); ?>>
 <div class="ribbon"></div>
 <div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">
    <aside class="right-off-canvas-menu" aria-hidden="true">
      <a role="search" class="mobile-search" title="Search" href="#" data-search-modal>Search</a>
      <div data-cart-open-container style="margin-left: 0; width: 0;">
        <a href="#" id="my-visit-opener-mobile" class="button button-table large" data-cart-open style="display: none;">
          <span class="first-icon-cell"><i class="icon icon-add-to-visit"></i></span><span class="text-cell">My&nbsp;Visit</span><span class="last-icon-cell"><i class="icon icon-anglebracket-right"></i></span>
        </a>
      </div>

      <?php wp_nav_menu( array(
        'theme_location' => 'primary', 
        'menu_class' => 'mobile-nav-global',
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'walker' => new wp_bootstrap_navwalker
      ) ); ?> 

      <ul class="nav-utility">
        <li><a class="nav-utility__link" title="English" href="#">English</a></li>
        <li><a class="nav-utility__link" title="French" href="#">French</a></li>
        <li><a class="nav-utility__link" title="Japnese" href="#">Japnese</a></li>
      </ul>
    </aside>
    <a class="exit-off-canvas"></a>
    <div class="logo-format">
     <?php  $custom_logo_id = get_theme_mod( 'custom_logo' ); 
     $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

     ?>
     <a href="<?php echo site_url();?>">
      <div class="header-logo-1"  style="background-image: url(<?php echo $logo[0]; ?>)">
      </div>
    </div>
  </a>
  <div id="header-outer-wrap">

    <header id="header" class="row">
      <div class="medium-24 columns">
       <?php  $custom_logo_id = get_theme_mod( 'custom_logo' ); 
       $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

       ?>
       <a href="<?php echo site_url();?>">
        <div class="site-id">
          <img  src="<?php echo $logo[0]; ?>" alt="abc" class="jmi-logo" width="120px">
          <img class="jmi5oth" src="/wp-content/themes/lz-child/img/jmi50th.png">
          
          <!-- <h2 class="jmi"><?php bloginfo('name'); ?></h2> -->
          <p class="group"><?php bloginfo('description'); ?></p>
        </div>
      </a>
      <a class="right-off-canvas-toggle mobile-menu" href="#" ></a>

      <?php //dynamic_sidebar( 'header-language' ); ?>
      <ul class="nav-utility inline-list">
        <!-- GTranslate: https://gtranslate.io/ -->

        <li class="nav-utility__item">  <a href="#" class="nav-utility__link" onclick="doGTranslate('en|en');return false;" title="English" class="glink nturl notranslate">English</a> </li>
        <li class="nav-utility__item">  <a href="#" class="nav-utility__link" onclick="doGTranslate('en|zh-CN');return false;" title="繁體中文" class="glink nturl notranslate">繁體中文</a> </li>
        <li class="nav-utility__item">  <a href="#" class="nav-utility__link" onclick="doGTranslate('en|ja');return false;" title="日本語" class="glink nturl notranslate">日本語</a> </li>
        <li class="nav-utility__item"><a role="search" data-search-modal="1" class="nav-utility__link navUtility-link" title="Search" href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>

        <style type="text/css">
        #goog-gt-tt {display:none !important;}
        .goog-te-banner-frame {display:none !important;}
        .goog-te-menu-value:hover {text-decoration:none !important;}
        .goog-text-highlight {background-color:transparent !important;box-shadow:none !important;}
        body {top:0 !important;}
        #google_translate_element2 {display:none!important;}
      </style>

      <div id="google_translate_element2"></div>
      <script type="text/javascript">
        function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
      </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


      <script type="text/javascript">
        function GTranslateGetCurrentLang() {var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');return keyValue ? keyValue[2].split('/')[2] : null;}
        function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}
        function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];if(GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0])return;var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(/goog-te-combo/.test(sel[i].className)){teCombo=sel[i];break;}if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
      </script>
    </ul>

    <nav class="nav-global" role="navigation">
      <?php wp_nav_menu( array(
        'theme_location' => 'primary', 
        'menu_class' => 'nav-global__list inline-list',
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'walker' => new wp_bootstrap_navwalker
      ) ); ?> 

        <div class="content explore about-jmi" style="width: 44.9rem">
        <div class="arrow-up"></div>
        <div class="column-1" style="width: 228px;">
          <nav class="dropdown-arrow-list" style="padding:0;">
            <ul>
              <?php 
              $global_nav =  get_pages(); 
              foreach ($global_nav as $key => $nav) {
               if( $nav->post_name == 'about-jmi'){
                $args = array(
                  'post_type'      => 'page',
                  'posts_per_page' => -1,
                  'post_parent'    => $nav->ID,
                  'order'          => 'ASC',
                  'orderby'        => 'menu_order'
                );
                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : ?>
                 <li><a>About JMI</a></li>
                 <?php while ( $parent->have_posts() ) : 

                  $parent->the_post(); ?>
                  <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>

              <?php endif;
              wp_reset_query(); 
            }
          } 
          ?>
        </ul>
      </nav>
    </div>

    <?php 
    $global_nav =  get_pages(); 
    foreach ($global_nav as $key => $nav) {
     if( $nav->post_name == 'about-jmi'){ ?>
      <div class="column-2" style="width: 490px !important;">
        <section id="c1550" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-left tai__image-intext tai__image-beside-text  ">
          <div class="tai__image-group-wrap large ">
            <div class="tai__image-group single-image">
              <img src="<?php echo get_the_post_thumbnail_url($nav->ID); ?>" alt="" class="lazyload"/>  
            </div>
          </div>
          <div class="ce-content rte-content rte-text-smaller " style="margin-bottom: 0px! important;">
            <?php echo get_the_excerpt($nav->ID);?>
            <p><a href="about-jmi" target="_self" class="internal-link arrow-link"><u>Read more</u></a></p>
          </div>
        </section>
        </div> <?php }} ?>

        <a tabindex="0" class="keyboard-close-menu" data-skip-link title="Exit menu" aria-label="Exit menu"><span>&times;</span></a>
      </div>


      <div class="content explore products">
        <div class="arrow-up"></div>
        <div class="column-1">
          <nav class="dropdown-arrow-list">
           <ul>
            <?php
            $args = array(
              'post_type' => 'product',
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'orderby'=> 'title', 'order' => 'ASC'
            );
            $loop = new WP_Query( $args ); ?>

            <li><a href="javascript:void();">Our Products</a></li>

            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

              <li><a href="<?php echo get_post_permalink(); ?>"><?php echo the_title();?></a></li>

            <?php  endwhile;
            wp_reset_query();               
            ?>
          </ul>
        </nav>
      </div>
      <?php 
      $global_nav =  get_pages(); 
      foreach ($global_nav as $key => $nav) {
        if( $nav->post_name == 'product'){   ?>
          <div class="column-2">
            <section id="c1550" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-left tai__image-intext tai__image-beside-text  ">
              <div class="tai__image-group-wrap large ">
                <div class="tai__image-group single-image">
                  <img src="<?php echo get_the_post_thumbnail_url($nav->ID); ?>" alt="" class="lazyload"/>
                </div>
              </div>
              <div class="ce-content rte-content rte-text-smaller " style="margin-bottom: 0px! important;margin-top: 2px;">
               <?php echo get_the_excerpt($nav->ID);?>
               <p><a href="product" target="_self" class="internal-link arrow-link"><u>Read more</u></a></p>
             </div>
           </section>
           </div> <?php }} ?>
           <a tabindex="0" class="keyboard-close-menu" data-skip-link title="Exit menu" aria-label="Exit menu"><span>&times;</span></a>
         </div>

         <div class="content explore professions">
          <div class="arrow-up"></div>
          <div class="column-1" style="padding: 15px 5px 15px 5px !important;">
            <nav class="dropdown-arrow-list">
              <ul>
                <?php
                $args = array(
                  'post_type' => 'professions',
                  'post_status' => 'publish',
                  'posts_per_page' => 4,
                  'orderby'=> 'title', 'order' => 'ASC'
                );
                $loop = new WP_Query( $args ); ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                  <li><a><?php echo the_title();?></a></li>

                <?php  endwhile;
                wp_reset_query();               
                ?>
              </ul>
            </nav>
            <nav class="dropdown-arrow-list">
              <ul>
                <?php
                $args = array(
                  'post_type' => 'professions',
                  'post_status' => 'publish',
                  'offset' => 4,
                  'posts_per_page' => 4,
                  'orderby'=> 'title', 'order' => 'ASC'
                );
                $loop = new WP_Query( $args ); ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                  <li><a href="<?php echo get_post_permalink(); ?>"><?php echo the_title();?></a></li>

                <?php  endwhile;
                wp_reset_query();               
                ?>
              </ul>
            </nav>
          <!--</div>-->
          <!--<div class="column-2 professions" style="padding: 15px 25px 15px 5px !important;">-->
            <nav class="dropdown-arrow-list">
              <ul>
                <?php
                $args = array(
                  'post_type' => 'professions',
                  'post_status' => 'publish',
                  'offset' => 8,
                  'posts_per_page' => 5,
                  'orderby'=> 'title', 'order' => 'ASC'
                );
                $loop = new WP_Query( $args ); ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                       <?php if( get_the_title() == 'Precision Injection Molding'): ?>
              
        <?php else: ?>
         <li><a href="<?php echo get_post_permalink(); ?>"><?php echo the_title();?></a></li>
        <?php endif;?>

                 

                <?php  endwhile;
                wp_reset_query();               
                ?>

              </ul>
              <p style="margin-bottom:0px;" ><a href="professions" target="_self" class="internal-link arrow-link see-all"><u>See All</u></a></p>
            </nav>
          </div>
          <a tabindex="0" class="keyboard-close-menu" data-skip-link title="Exit menu" aria-label="Exit menu"><span>&times;</span></a>
        </div>

        <div class="content explore news">
          <div class="arrow-up"></div>
          <?php 
          $global_nav =  get_pages(); 
          foreach ($global_nav as $key => $nav) {
           if( $nav->post_name == 'news-updates'){ ?>
            <div class="column-1">
              <section id="c1550" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-left tai__image-intext tai__image-beside-text  ">
                <div class="tai__image-group-wrap large news-img">
                  <div class="tai__image-group single-image">
                    <img src="<?php echo get_the_post_thumbnail_url($nav->ID); ?>" alt="" class="lazyload"/>
                  </div>
                </div>
                <div class="ce-content rte-content rte-text-smaller " style="margin-bottom: 8px! important; font-size: 13px; line-height: 15px! important;">
                 <?php echo get_the_excerpt($nav->ID);?>
                 <a href="news-updates" target="_self" class="internal-link arrow-link"><u>Read more</u></a>
               </div>
             </section>
             </div> <?php }}?>
          <div class="column-2" style="padding-bottom: 7px! important; display:block !important" >
              <nav class="dropdown-arrow-list">
                <ul>
                  <?php 
                  $args = array(
                    'type'                     => 'post', /* custom post type name */
                    'parent'                   => '',
//                    'orderby'                  => 'id',
//                    'order'                    => 'ASC',
//                    'hide_empty'               => 1,
//                    'hierarchical'             => 1,   
                    'taxonomy'                 => 'category'  /* custom post type texonomy name */
                  ); 
                  $cats = get_categories($args);
                  echo '  <li style="padding-bottom: 8px"><a href="javascript:void();">News and Updates</a></li>';
                  foreach ($cats as $cat) {  
                    $cat_id= $cat->term_id;
                    if( $cat->slug == 'jmi-corporate-news'){
                      echo'<li style="padding-bottom: 8px"><a href="'.get_category_link($cat_id).'">JMI Corporate News</a></li>';
                    }elseif($cat->slug == 'csr-events') {
                   
                      echo'<li style="padding-bottom: 8px"><a href="'.get_category_link($cat_id).'">JMI CSR Events</a></li>';
                    }else{
                     echo '<li style="padding-bottom: 8px"><a href="'.get_category_link($cat_id).'">'.$cat->name.'</a></li>';
                    }
                   
                    
                   ?>
                    
                  <?php  } ?>
                </ul>

              </nav>
            </div>
            <a tabindex="0" class="keyboard-close-menu" data-skip-link title="Exit menu" aria-label="Exit menu"><span>&times;</span></a>
          </div>

          <div class="content explore contact" >
            <div class="arrow-up"></div>
            <?php 
            $global_nav =  get_pages(); 
            foreach ($global_nav as $key => $nav) {
              if( $nav->post_name == 'contact-us'){   ?>
                <div class="column-1" style="width:401px! important;">
                  <section id="c1550" class="text-and-images ce-margin-top-0 ce-margin-bottom-0 tai__ tai__image-left tai__image-intext tai__image-beside-text  ">
                    <div class="ce-content rte-content rte-text-smaller "  style="margin-bottom: 0px! important; " >
                     <?php echo $nav->post_excerpt; ?>

                   </div>
                 </section>
               </div>
               <div class="column-2" style="padding:0px! important; width:265px! important;">
                <div class="tai__image-group-wrap large contact-img">
                  <div class="tai__image-group single-image">

                    <img src="<?php echo get_the_post_thumbnail_url($nav->ID); ?>" alt="" class="lazyload"/>
                  </div>
                  <a href="contact-us" target="_self" class="internal-link arrow-link get-quote"><u>Get a Quote</u></a>
                </div>
                <!--</section>-->
              </div>
            <?php }}?> 
            <a tabindex="0" class="keyboard-close-menu" data-skip-link title="Exit menu" aria-label="Exit menu"><span>&times;</span></a>
          </div>

        </nav>
        <div class="breadcrumb-share-wrap">
          <div class="breadcrumb">
          </div>
        </div>
      </div><!-- end .columns -->
    </header><!-- end .row -->
  </div><!-- end #header-outer-wrap-->
