<?php
/**
 * Template Name: News updates Page
 * The template for displaying news updates Page.
 *
 *
 * @package lz Team
 */
get_header();

$bannerimg = get_field('banner_image');
$bannertext = get_field('banner_text');
$quotetext = get_field('get_a_quote_text');
$quoteurl = get_field('get_a_quote_url');
?>
<div class="inner-wrap medium-24 news-update">
    <div class="banner-section">
        <img src="<?php echo $bannerimg['url']; ?>">

       
        <div class="feature-footer" style="left: 907.688px; position: relative;">
            <div class="inner-wrap">
                <a class="plan-button button button-table large" href="contact-us">
                    <span class="first-icon-cell get-first">
                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                    </span>
                    <span class="text-cell text">Get a Quote</span>
                    <span class="last-icon-cell get-last">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div><!-- end .inner-wrap -->
        </div>
    </div><!-- banner-section end -->
    <div class="row news-section">
        <div class="columns news-columns">
            <div class="lc__intro-content">
<?php
$blogtime = date('Y');
$prev_limit_year = $blogtime - 1;
$prev_month = '';
$prev_year = '';
$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'order' => 'DESC',
    'posts_per_page' => -1 // this will retrive all the post that is published 
);
//   $loop = new WP_Query( $args ); 


$postsbymonth = new WP_Query($args);

while ($postsbymonth->have_posts()) {

    $postsbymonth->the_post();

    if (get_the_time('F') != $prev_month || get_the_time('Y') != $prev_year && get_the_time('Y') == $prev_limit_year) {


        echo "<hr>";
        echo "<h2>" . get_the_time('F') . "</h2>\n\n";
    }


    $cats = get_the_category(get_the_ID());
    $catArr = [];
    $cat_url = [];
    $cat_slug = [];
    foreach ($cats as $key => $cat) {
        $catArr = $cat->name;
        $cat_url = get_category_link($cat->term_id);
        $cat_slug = $cat->category_nicename;
    }
    ?>
                    <hr>
                    <h3><?php echo get_the_date('M j, Y'); ?>&nbsp;&nbsp; <a href="<?php echo $cat_url; ?>"><span class="<?php echo $cat_slug; ?>"><?php echo $catArr; ?></span></a>&nbsp;&nbsp;<a href="<?php the_permalink(); ?>">/&nbsp;<?php the_excerpt(); ?></a></h3>

                    <?php // your other template tags ?>


    <?php
    $prev_month = get_the_time('F');
    $prev_year = get_the_time('Y');
}
?>


            
                <div class="inner-news-content">




                </div>

<?php wp_reset_query(); ?>
            </div>

      <div class="lc__sidebar text-light">

    <?php if (is_active_sidebar('news-update-sidebar')) : ?>
        <?php dynamic_sidebar('news-update-sidebar'); ?>
    <?php endif; ?>

            </div>
        </div>
    </div>
</div>

                <?php get_footer(); ?>