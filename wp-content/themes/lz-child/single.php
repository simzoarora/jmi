<?php
/**
 * The Template for displaying all single posts.
 *
 * @package SKT Blendit
 */
get_header();
?>

<div class="inner-wrap medium-24 news-update">
    <div class="banner-section" style="background-image: url(<?php echo get_the_post_thumbnail_url();  ?>)">
        <div class="banner-text-section">
            <div class="banner-text-container row">
                <h6><?php the_title(); ?></h6>
                <h1 tabindex="-1" id="keep_up_to_date_with_our_br_news_and_updates_0">Keep Up To Date With Our <br> News And Updates</h1>
            </div></div>
    </div><!-- banner-section end -->


    <div class="row news-section">
        <div class="feature-footer" style="position: relative !important;">
            <div class="inner-wrap">
                <a class="plan-button button button-table large" href="contact-us" style="height: 70px;">
                    <span class="first-icon-cell get-first">
                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                    </span>
                    <span class="text-cell text">Get a Quote</span>
                    <span class="last-icon-cell get-last">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </span>
                </a>
            </div><!-- end .inner-wrap -->
        </div>
        <div class="columns news-columns">
            <div class="lc__intro-content single-news-content">
                <?php while (have_posts()) : the_post(); ?>
                    <h2 class="single-news-title"> <?php echo the_title(); ?> <br>
                        <span class="single-news-date"><?php echo get_the_date(); ?></span> 
                    </h2>
                    <p><?php the_content(); ?></p>

                <?php endwhile; // end of the loop. ?>          

            </div>


            <div class="lc__sidebar text-light">

                <?php if (is_active_sidebar('news-update-sidebar')) : ?>
                    <?php dynamic_sidebar('news-update-sidebar'); ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>